package com.brainmagic.covid19helpdesk.adapter;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.brainmagic.covid19helpdesk.R;
import com.brainmagic.covid19helpdesk.model.departmentdetails.DepartmentDetailsModel;
import com.brainmagic.covid19helpdesk.model.departmentdetails.DepartmentDetailsResult;
import com.brainmagic.covid19helpdesk.model.usefulllinks.UseFullData;

import java.text.DateFormatSymbols;
import java.util.List;

public class WareHouseAdapter extends RecyclerView.Adapter<WareHouseAdapter.ViewHolder> {

    private Context context;
    private List<DepartmentDetailsResult> wareHouseData;

    public WareHouseAdapter(Context context, List<DepartmentDetailsResult> wareHouseData) {
        this.context = context;
        this.wareHouseData = wareHouseData;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.view_ware_house_details_adapter, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        DepartmentDetailsResult date = wareHouseData.get(position);
        String datatime = date.getDate();
        String datesplit[] = datatime.split("T");
        String spliteddate = datesplit[0];
        String dateonly[] = spliteddate.split("-");

        String monthnumber = dateonly[1];
        String month = new DateFormatSymbols().getMonths()[Integer.parseInt(monthnumber) - 1];
        String monthget = month.substring(0, 3);
        String year = dateonly[0];
        String yearend = year.substring(2, 4);
        String datefinal = dateonly[2] + "-" + monthget + "-" + yearend;
        holder.date.setText(datefinal);
        holder.mobileNo.setText(date.getMobileNo());

        holder.mobileNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent openlink = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+wareHouseData.get(position).getMobileNo()));
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    ActivityCompat.requestPermissions((Activity) context,new String[]{Manifest.permission.CALL_PHONE},200);
                    return;
                }
                else {

                }
                context.startActivity(openlink);
            }
        });
    }

    @Override
    public int getItemCount() {
        return wareHouseData.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{


        private TextView date, mobileNo;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            date=itemView.findViewById(R.id.ware_house_date_tv);
            mobileNo =itemView.findViewById(R.id.ware_house_phone);
        }
    }

}




package com.brainmagic.covid19helpdesk.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.brainmagic.covid19helpdesk.R;
import com.brainmagic.covid19helpdesk.alert.AlertBox;
import com.brainmagic.covid19helpdesk.alert.TransparentProgressDialog;
import com.brainmagic.covid19helpdesk.model.api.APIService;
import com.brainmagic.covid19helpdesk.model.api.RetroClient;
import com.brainmagic.covid19helpdesk.model.removeproviders.DeleteProviders;
import com.brainmagic.covid19helpdesk.model.viewproviders.ViewProviders;
import com.brainmagic.covid19helpdesk.model.viewproviders.ViewProvidersData;

import java.text.DateFormatSymbols;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewProviderAdapter extends RecyclerView.Adapter<ViewProviderAdapter.ViewHolder> {

    private Context context;
    List<ViewProvidersData> viewProvidersData;
    private String providerType;

    public ViewProviderAdapter(Context context, List<ViewProvidersData> viewProvidersData, String providerType){
        this.context=context;
        this.viewProvidersData=viewProvidersData;
        this.providerType=providerType;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.view_provider_adapter,parent,false);
        ViewHolder viewHolder=new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {


        String datatime=viewProvidersData.get(position).getDate();
        String datesplit[]=datatime.split("T");
        String spliteddate=datesplit[0];
        String dateonly[]=spliteddate.split("-");

        String monthnumber=dateonly[1];
        String month = new DateFormatSymbols().getMonths()[Integer.parseInt(monthnumber)-1];
        String monthget=month.substring(0, 3);
        String year=dateonly[0];
        String yearend=year.substring(2,4);
        String datefinal=dateonly[2]+"-"+monthget+"-"+yearend;
        holder.provider_date_tv.setText(datefinal);
        holder.provider_name_tv.setText(viewProvidersData.get(position).getNameofOrganization());
        holder.provider_details_tv.setText(viewProvidersData.get(position).getProvide());
        if (providerType.equals("Provider")){
            holder.delete_provider_self.setVisibility(View.VISIBLE);
        }else {
            holder.delete_provider_self.setVisibility(View.GONE);
        }

        holder.delete_provider_self.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteProviderDetails(viewProvidersData.get(position).getId(),viewProvidersData.get(position).getRegId(),position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return viewProvidersData.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView provider_details_tv,provider_name_tv,provider_date_tv;
        private ImageView delete_provider_self;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            provider_details_tv=itemView.findViewById(R.id.provider_details_tv);
            provider_name_tv=itemView.findViewById(R.id.provider_name_tv);
            provider_date_tv=itemView.findViewById(R.id.provider_date_tv);
            delete_provider_self=itemView.findViewById(R.id.delete_provider_self);
        }
    }

    private void deleteProviderDetails(final int id, String regId, final int position)
    {
        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(context, R.layout.custom_progress_bar);
        progressDialog.show();
        try{
            APIService service = RetroClient.getApiService();
            final Call<DeleteProviders> call = service.deletProvides(regId,id);
            call.enqueue(new Callback<DeleteProviders>() {
                @Override
                public void onResponse(Call<DeleteProviders> call, Response<DeleteProviders> response) {
                    progressDialog.dismiss();
                    try{
                        if("Success".equalsIgnoreCase(response.body().getResult()))
                        {

                            viewProvidersData.remove(position);
                            notifyDataSetChanged();

                        }
                        else {
                            AlertBox alertBox = new AlertBox(context);
                            alertBox.showAlertBox(context.getString(R.string.tryagainlater),View.GONE);

                        }
                    }
                    catch (Exception e)
                    {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        AlertBox alertBox = new AlertBox(context);
                        alertBox.showAlertBox(context.getString(R.string.invalid_response),View.GONE);

                    }
                }
                @Override
                public void onFailure(Call<DeleteProviders> call, Throwable t) {
                    progressDialog.dismiss();
                    final AlertBox alertBox = new AlertBox(context);
                    alertBox.showAlertBox(context.getString(R.string.failed_to_reach_server),View.GONE);

                }
            });
        }catch (Exception e)
        {
            progressDialog.dismiss();
            AlertBox alertBox = new AlertBox(context);
            alertBox.showAlertBox(context.getString(R.string.cannot_load_data),View.GONE);
        }

    }

}




package com.brainmagic.covid19helpdesk.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.brainmagic.covid19helpdesk.R;
import com.brainmagic.covid19helpdesk.spinner.MultiSelectionSpinner;

import java.util.List;

public class ComaSeparatorAdapter extends ArrayAdapter {
    private Context context;
    private int resource;
    public ComaSeparatorAdapter(@NonNull Context context, int resource) {
        super(context, resource);
        this.context = context;
        this.resource = resource;
    }

    public ComaSeparatorAdapter(@NonNull Context context, int resource, int textViewResourceId) {
        super(context, resource, textViewResourceId);
    }

    public ComaSeparatorAdapter(@NonNull Context context, int resource, @NonNull Object[] objects) {
        super(context, resource, objects);
    }

    public ComaSeparatorAdapter(@NonNull Context context, int resource, int textViewResourceId, @NonNull Object[] objects) {
        super(context, resource, textViewResourceId, objects);
    }

    public ComaSeparatorAdapter(@NonNull Context context, int resource, @NonNull List objects) {
        super(context, resource, objects);
    }

    public ComaSeparatorAdapter(@NonNull Context context, int resource, int textViewResourceId, @NonNull List objects) {
        super(context, resource, textViewResourceId, objects);
    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView == null)
           convertView = LayoutInflater.from(context).inflate(resource, parent, false);
        TextView view = (TextView) convertView; //if layout has only text view
        if(view == null) return null;
        if(!TextUtils.isEmpty(((MultiSelectionSpinner) parent)._itemsAtStart) )
        {
            view.setTextColor(context.getResources().getColor(R.color.primary));
            view.setText(((MultiSelectionSpinner) parent)._itemsAtStart);
        }
        else {
            view.setTextColor(context.getResources().getColor(R.color.grey));
            view.setText(((MultiSelectionSpinner) parent).hint);
        }

        return view;
    }


}

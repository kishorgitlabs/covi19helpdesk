package com.brainmagic.covid19helpdesk.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.brainmagic.covid19helpdesk.R;
import com.brainmagic.covid19helpdesk.model.latestnews.LatestNewsData;

import java.util.List;

public class LatestNewsAdapter extends RecyclerView.Adapter<LatestNewsAdapter.ViewHolder> {

    private Context context;
    private List<LatestNewsData> latestNewsAdapterList;

    public LatestNewsAdapter(Context context, List<LatestNewsData> latestNewsAdapterList){
        this.context=context;
        this.latestNewsAdapterList=latestNewsAdapterList;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.latest_news_adapter,parent,false);
        ViewHolder viewHolder=new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        holder.news.setText(latestNewsAdapterList.get(position).getLatestNewslink());
        holder.newsheading.setText(latestNewsAdapterList.get(position).getTopic());


    }

    @Override
    public int getItemCount() {
        return latestNewsAdapterList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{


        private TextView news,newsheading;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            news=itemView.findViewById(R.id.news);
            newsheading=itemView.findViewById(R.id.newsheading);
        }
    }

}




package com.brainmagic.covid19helpdesk.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.brainmagic.covid19helpdesk.Constants;
import com.brainmagic.covid19helpdesk.R;
import com.brainmagic.covid19helpdesk.alert.AlertBox;
import com.brainmagic.covid19helpdesk.alert.TransparentProgressDialog;
import com.brainmagic.covid19helpdesk.model.api.APIService;
import com.brainmagic.covid19helpdesk.model.api.RetroClient;
import com.brainmagic.covid19helpdesk.model.updateneeds.UpdateNeedyModel;
import com.brainmagic.covid19helpdesk.model.viewneeds.ViewNeedsModel;
import com.brainmagic.covid19helpdesk.model.viewneeds.ViewNeedsResult;
import com.brainmagic.covid19helpdesk.network.NetworkConnection;

import java.text.DateFormatSymbols;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewNeedyAdapter extends RecyclerView.Adapter<ViewNeedyAdapter.ViewHolder> {

    private Context context;
    private List<ViewNeedsResult> data;
    private String need;
    private SharedPreferences sharedPref;


    public ViewNeedyAdapter(Context context, List<ViewNeedsResult> data, String need){
        this.context=context;
        this.data = data;
        this.need = need;
    }

    public void setData(List<ViewNeedsResult> data)
    {
        this.data = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.view_needs_adapter,parent,false);
        sharedPref = context.getSharedPreferences(Constants.sharedConfig, Context.MODE_PRIVATE);
        ViewHolder viewHolder=new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        String datatime=data.get(position).getDate();
        String dateSplit[]=datatime.split("T");
        String splitedDate=dateSplit[0];
        String dateOnly[]=splitedDate.split("-");

        String monthNumber=dateOnly[1];
        String month = new DateFormatSymbols().getMonths()[Integer.parseInt(monthNumber)-1];
        String monthGet=month.substring(0, 3);
        String year=dateOnly[0];
        String yearEnd=year.substring(2,4);
        String dateFinal=dateOnly[2]+"-"+monthGet+"-"+yearEnd;
        holder.date.setText(dateFinal);
        holder.needs.setText(data.get(position).getIneed());
        if("needy".equalsIgnoreCase(need))
        {
            holder.closeRequestIv.setVisibility(View.GONE);
            holder.nameLayoutRl.setVisibility(View.VISIBLE);
            holder.name.setText(data.get(position).getName());
        }
        else
        {
            holder.nameLayoutRl.setVisibility(View.GONE);
            holder.closeRequestIv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertBox alertBox = new AlertBox(context);
                    alertBox.showAlertBoxWithEt(context.getString(R.string.close_request_msg), View.VISIBLE);
                    alertBox.setOnMessageClickListener(new AlertBox.OnMessagePassClickListener() {
                        @Override
                        public void onClick(String msg) {
                            NetworkConnection connection = new NetworkConnection(context);
                            if(connection.checkInternet())
                            {
                                updateMyNeed(msg, position, data.get(position).getId());
                            }
                            else {

                                AlertBox alertBox = new AlertBox(context);
                                alertBox.showAlertBox(context.getString(R.string.no_internet_connection), View.GONE);
                            }

                        }
                    });
                }
            });
        }


    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView date, name, needs;
        ImageView closeRequestIv;
        RelativeLayout nameLayoutRl;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.date_tv);
            name = itemView.findViewById(R.id.name_tv);
            needs = itemView.findViewById(R.id.details_tv);
            closeRequestIv = itemView.findViewById(R.id.close_request_im);
            nameLayoutRl = itemView.findViewById(R.id.name_layout_rl);
        }
    }

    private void updateMyNeed(String remarks, final int pos, String id)
    {
        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(context, R.layout.custom_progress_bar);
        progressDialog.show();
        try{
            APIService service = RetroClient.getApiService();
            Call<UpdateNeedyModel> call = service.updateMyNeed(sharedPref.getString("tokenId",""),id,remarks);
            call.enqueue(new Callback<UpdateNeedyModel>() {
                @Override
                public void onResponse(Call<UpdateNeedyModel> call, Response<UpdateNeedyModel> response) {
                    progressDialog.dismiss();
                    try{
                        if(response.isSuccessful())
                        {
                            if("success".equalsIgnoreCase(response.body().getResult()))
                            {
                                AlertBox alertBox = new AlertBox(context);
                                alertBox.showAlertBoxWithListener(context.getString(R.string.request_close_msg), View.GONE);
                                alertBox.setOnPositiveClickListener(new AlertBox.OnPositiveClickListener() {
                                    @Override
                                    public void onPositiveClick() {
                                        data.remove(pos);
                                        notifyDataSetChanged();
                                    }
                                });
                            }
                            else {
                                AlertBox alertBox = new AlertBox(context);
                                alertBox.showAlertBox(context.getString(R.string.incomplete_request), View.GONE);
                            }

                        }
                        else {
                            AlertBox alertBox = new AlertBox(context);
                            alertBox.showAlertBox(context.getString(R.string.invalid_url), View.GONE);
                        }
                    }
                    catch (Exception e)
                    {
                        progressDialog.dismiss();
                        AlertBox alertBox = new AlertBox(context);
                        alertBox.showAlertBox(context.getString(R.string.invalid_response), View.GONE);
                    }
                }

                @Override
                public void onFailure(Call<UpdateNeedyModel> call, Throwable t) {
                    progressDialog.dismiss();
                    AlertBox alertBox = new AlertBox(context);
                    alertBox.showAlertBox(context.getString(R.string.failed_to_reach_server), View.GONE);
                }
            });
        }
        catch (Exception e)
        {
            progressDialog.dismiss();
            AlertBox alertBox = new AlertBox(context);
            alertBox.showAlertBox(context.getString(R.string.cannot_connect_to_server), View.GONE);
        }
    }

}




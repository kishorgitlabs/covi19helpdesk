package com.brainmagic.covid19helpdesk.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatSpinner;

import com.brainmagic.covid19helpdesk.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ThemeColorAdapter extends ArrayAdapter {
    private Context context;
    private int res;
    List<String> objects;
    public ThemeColorAdapter(@NonNull Context context, int resource) {
        super(context, resource);
        this.context = context;
        this.res = resource;
    }

    public ThemeColorAdapter(@NonNull Context context, int resource, int textViewResourceId) {
        super(context, resource, textViewResourceId);
        this.context = context;
        this.res = resource;
    }

    public ThemeColorAdapter(@NonNull Context context, int resource, @NonNull String[] objects) {
        super(context, resource, objects);
        this.context = context;
        this.res = resource;
        this.objects = new ArrayList<>(Arrays.asList(objects));
    }

    public ThemeColorAdapter(@NonNull Context context, int resource, int textViewResourceId, @NonNull Object[] objects) {
        super(context, resource, textViewResourceId, objects);
        this.context = context;
        this.res = resource;
    }

    public ThemeColorAdapter(@NonNull Context context, int resource, @NonNull List objects) {
        super(context, resource, objects);
        this.context = context;
        this.res = resource;
        this.objects = objects;
    }

    public ThemeColorAdapter(@NonNull Context context, int resource, int textViewResourceId, @NonNull List objects) {
        super(context, resource, textViewResourceId, objects);
        this.context = context;
        this.res = resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView == null)
        {
            convertView = LayoutInflater.from(context).inflate(res, parent, false);
        }
        TextView view = (TextView) convertView;
        if(view == null) return super.getView(position, convertView, parent);

        if(objects != null) {
            view.setText(objects.get(position));
            if (objects.get(position).contains("Choose") || objects.get(position).contains("Select")|| objects.get(position).contains("Occupation") ) {
                view.setTextColor(context.getResources().getColor(R.color.grey));
            }
            else {
                view.setTextColor(context.getResources().getColor(R.color.primary));
            }
        }

        return view;
    }
}

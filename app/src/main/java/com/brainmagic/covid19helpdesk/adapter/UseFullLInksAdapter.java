package com.brainmagic.covid19helpdesk.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.brainmagic.covid19helpdesk.R;
import com.brainmagic.covid19helpdesk.model.usefulllinks.UseFullData;

import java.util.List;

public class UseFullLInksAdapter  extends RecyclerView.Adapter<UseFullLInksAdapter.ViewHolder> {

    private Context context;
    private List<UseFullData> useFullDataList;

    public UseFullLInksAdapter(Context context, List<UseFullData> useFullDataList){
        this.context=context;
        this.useFullDataList=useFullDataList;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.useful_links_adapter,parent,false);
        ViewHolder viewHolder=new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        holder.linktopic.setText(useFullDataList.get(position).getTopic());

        holder.usefulllinklayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent openlink = new Intent(Intent.ACTION_VIEW, Uri.parse(useFullDataList.get(position).getLink()));
                context.startActivity(openlink);
            }
        });
    }

    @Override
    public int getItemCount() {
        return useFullDataList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{


        private TextView linktopic;
        private RelativeLayout usefulllinklayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            linktopic=itemView.findViewById(R.id.linktopic);
            usefulllinklayout=itemView.findViewById(R.id.usefulllinklayout);
        }
    }

}




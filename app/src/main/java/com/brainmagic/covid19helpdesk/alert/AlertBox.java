package com.brainmagic.covid19helpdesk.alert;



import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.brainmagic.covid19helpdesk.R;
import com.brainmagic.covid19helpdesk.adapter.ThemeColorAdapter;
import com.brainmagic.covid19helpdesk.model.StringListModel;
import com.brainmagic.covid19helpdesk.model.StringModel;
import com.goodiebag.pinview.Pinview;

import java.util.List;


/**
 * Created by Systems02 on 22-May-17.
 */

public class AlertBox {

    public Context context;
    public AlertDialog alertDialog;
    private OnPositiveClickListener onPositiveClickListener;
    private OnNegativeClickListener onNegativeClickListener;
    private OnCustomClickListener onCustomClickListener;
    private OnMessagePassClickListener onMessagePassClickListener;
    private OnMessageValuePassClickListener onMessageValuePassClickListener;
    private Spinner spState, spCity, spDep;

    public AlertBox(Context context) {
        this.context = context;
//        onClickListener= (Alert.onClickListener) context;
    }


    public void showSearchableAlertBox() {
        alertDialog = new AlertDialog.Builder(
                context).create();

        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.searchable_alert_box, null);
        spState = dialogView.findViewById(R.id.sp_state);
        spCity = dialogView.findViewById(R.id.sp_city);
        spDep = dialogView.findViewById(R.id.sp_department);
        Button okayBt = (Button)  dialogView.findViewById(R.id.okay_bt);
        Button cancelBt = (Button)  dialogView.findViewById(R.id.cancel_bt);
        LinearLayout cancelLayout =  dialogView.findViewById(R.id.cancel_layout);
        cancelLayout.setVisibility(View.VISIBLE);
        alertDialog.setView(dialogView);

        spCity.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    onMessagePassClickListener.onClick(spState.getSelectedItem().toString());
                }
                    return false;
                }
        });

        spDep.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_UP) {
                onCustomClickListener.onClick();
            }
            return false;
            }
        });

        okayBt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                onMessageValuePassClickListener.onClick(alertDialog,spState.getSelectedItem().toString(),spCity.getSelectedItem().toString(),spDep.getSelectedItem().toString());


            }
        });
        cancelBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                alertDialog.dismiss();

            }
        });
        //alertDialog.setMessage(msg);
        //alertDialog.setTitle("Turbo Energy Limited");
       /* alertDialog.setIcon(ContextCompat.getDrawable(context,R.mipmap.ic_launcher_square));
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });*/
        //alertDialog.setIcon(R.drawable.logo);
        alertDialog.show();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        /*alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        })*/
    }

    public void setStateList(List<String> stateList)
    {
        stateList.add(0,"Choose State");
        ThemeColorAdapter arrayAdapter = new ThemeColorAdapter(context, R.layout.support_simple_spinner_dropdown_item,stateList);
        spState.setAdapter(arrayAdapter);
    }

    public void setCityList(List<String> cityList)
    {
        cityList.add(0,"Choose City");
        ThemeColorAdapter arrayAdapter = new ThemeColorAdapter(context, R.layout.support_simple_spinner_dropdown_item,cityList);
        spCity.setAdapter(arrayAdapter);
    }
    public void setDepList(List<String> depList)
    {
        depList.add(0,"Choose Department");
        ThemeColorAdapter arrayAdapter = new ThemeColorAdapter(context, R.layout.support_simple_spinner_dropdown_item,depList);
        spDep.setAdapter(arrayAdapter);
    }


    public void showAlertBox(String msg, int isVisible) {
        alertDialog = new AlertDialog.Builder(
                context).create();

        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alert_box_msg, null);
        TextView alert = dialogView.findViewById(R.id.alert_msg);
        alert.setText(msg);
        Button okayBt = (Button)  dialogView.findViewById(R.id.okay_bt);
        EditText RemarkEt =  dialogView.findViewById(R.id.remark_et);
//        Button cancelBt = (Button)  dialogView.findViewById(R.id.cancel_bt);
//        LinearLayout cancelBtLayout =  dialogView.findViewById(R.id.cancel_layout);
//        RemarkEt.setVisibility(isVisible);
        alertDialog.setView(dialogView);
        okayBt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                alertDialog.dismiss();

            }
        });
        //alertDialog.setMessage(msg);
        //alertDialog.setTitle("Turbo Energy Limited");
       /* alertDialog.setIcon(ContextCompat.getDrawable(context,R.mipmap.ic_launcher_square));
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });*/
        //alertDialog.setIcon(R.drawable.logo);
        alertDialog.show();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        /*alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        })*/
    }
    public void showAlertBoxWithEt(String msg, int isVisible) {
        alertDialog = new AlertDialog.Builder(
                context).create();

        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alert_box_msg, null);
        TextView alert = dialogView.findViewById(R.id.alert_msg);
        alert.setText(msg);
        Button okayBt = (Button)  dialogView.findViewById(R.id.okay_bt);
        final EditText remarkEt =  dialogView.findViewById(R.id.remark_et);
//        Button cancelBt = (Button)  dialogView.findViewById(R.id.cancel_bt);
//        LinearLayout cancelBtLayout =  dialogView.findViewById(R.id.cancel_layout);
        remarkEt.setVisibility(isVisible);
        alertDialog.setView(dialogView);
        okayBt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                alertDialog.dismiss();
                onMessagePassClickListener.onClick(remarkEt.getText().toString());

            }
        });
        //alertDialog.setMessage(msg);
        //alertDialog.setTitle("Turbo Energy Limited");
       /* alertDialog.setIcon(ContextCompat.getDrawable(context,R.mipmap.ic_launcher_square));
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });*/
        //alertDialog.setIcon(R.drawable.logo);
        alertDialog.show();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        /*alertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {

            }
        })*/
    }
    public void showAlertBoxWithListener(String msg, int isVisible) {
        alertDialog = new AlertDialog.Builder(
                context).create();

        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.alert_box_msg, null);
        TextView alert = dialogView.findViewById(R.id.alert_msg);
        alert.setText(msg);
        Button okayBt = (Button)  dialogView.findViewById(R.id.okay_bt);
        Button cancelBt = (Button)  dialogView.findViewById(R.id.cancel_bt);
        LinearLayout cancelLayout = dialogView.findViewById(R.id.cancel_layout);
        alertDialog.setView(dialogView);
        cancelLayout.setVisibility(isVisible);
        okayBt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                alertDialog.dismiss();
//                ((Activity) context).finish();

                onPositiveClickListener.onPositiveClick();
            }
        });

        cancelBt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                alertDialog.dismiss();
//                ((Activity) context).finish();
                onNegativeClickListener.onPositiveClick();
            }
        });
        alertDialog.show();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
    }
    public void listenerWithPinView(String msg) {
        alertDialog = new AlertDialog.Builder(
                context).create();

        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.pin_view_alert, null);
        TextView resendOtpTv = dialogView.findViewById(R.id.resend_otp_tv);
        final Pinview otpPinView = dialogView.findViewById(R.id.otp_pv);
        Button okayBt = (Button)  dialogView.findViewById(R.id.okay_bt);
        Button cancelBt = (Button)  dialogView.findViewById(R.id.cancel_bt);
        alertDialog.setView(dialogView);
        okayBt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                alertDialog.dismiss();
//                ((Activity) context).finish();
                onMessagePassClickListener.onClick(otpPinView.getValue());
            }
        });

        resendOtpTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCustomClickListener.onClick();
            }
        });

        cancelBt.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                alertDialog.dismiss();
//                ((Activity) context).finish();
            }
        });
        alertDialog.show();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
    }

    public void setOnMessageClickListener(OnMessagePassClickListener onClickListener){
        this.onMessagePassClickListener=onClickListener;
    }

    public void setOnPositiveClickListener(OnPositiveClickListener onClickListener){
        this.onPositiveClickListener=onClickListener;
    }

    public void setOnNegativeClickListener(OnNegativeClickListener onClickListener){
        this.onNegativeClickListener=onClickListener;
    }

    public void setOnCustomClickListener(OnCustomClickListener onClickListener){
        this.onCustomClickListener=onClickListener;
    }


    public void setOnValueClickListener(OnMessageValuePassClickListener onClickListener){
        this.onMessageValuePassClickListener=onClickListener;
    }

    public interface OnPositiveClickListener{
        public void onPositiveClick();
    }

    public interface OnNegativeClickListener{
        public void onPositiveClick();
    }

    public interface OnCustomClickListener{
        public void onClick();
    }

    public interface OnMessagePassClickListener{
        public void onClick(String msg);
    }



    public interface OnMessageValuePassClickListener{
        public void onClick(AlertDialog alertDialog, String state, String city, String dep);
    }
}

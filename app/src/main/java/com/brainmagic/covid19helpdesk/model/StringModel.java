
package com.brainmagic.covid19helpdesk.model;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class StringModel {

    @SerializedName("data")
    private String mData;
    @SerializedName("result")
    private String mResult;

    public String getData() {
        return mData;
    }

    public void setData(String data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}

package com.brainmagic.covid19helpdesk.model.latestnews;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class LatestNews{

	@SerializedName("result")
	private String result;

	@SerializedName("data")
	private List<LatestNewsData> data;

	public void setResult(String result){
		this.result = result;
	}

	public String getResult(){
		return result;
	}

	public void setData(List<LatestNewsData> data){
		this.data = data;
	}

	public List<LatestNewsData> getData(){
		return data;
	}

	@Override
 	public String toString(){
		return 
			"LatestNews{" + 
			"result = '" + result + '\'' + 
			",data = '" + data + '\'' + 
			"}";
		}
}

package com.brainmagic.covid19helpdesk.model.viewneeds;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ViewNeedsModel {

    @SerializedName("data")
    private List<ViewNeedsResult> mData;
    @SerializedName("result")
    private String mResult;

    public List<ViewNeedsResult> getData() {
        return mData;
    }

    public void setData(List<ViewNeedsResult> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}


package com.brainmagic.covid19helpdesk.model.registration;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class RegistrationModel {

    @SerializedName("data")
    private RegistrationResult mData;
    @SerializedName("result")
    private String mResult;

    public RegistrationResult getData() {
        return mData;
    }

    public void setData(RegistrationResult data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}

package com.brainmagic.covid19helpdesk.model.usefulllinks;

import com.google.gson.annotations.SerializedName;

public class UseFullData {

	@SerializedName("InsertDate")
	private String insertDate;

	@SerializedName("id")
	private int id;

	@SerializedName("Topic")
	private String topic;

	@SerializedName("Link")
	private String link;

	public void setInsertDate(String insertDate){
		this.insertDate = insertDate;
	}

	public String getInsertDate(){
		return insertDate;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setTopic(String topic){
		this.topic = topic;
	}

	public String getTopic(){
		return topic;
	}

	public void setLink(String link){
		this.link = link;
	}

	public String getLink(){
		return link;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"insertDate = '" + insertDate + '\'' + 
			",id = '" + id + '\'' + 
			",topic = '" + topic + '\'' + 
			",link = '" + link + '\'' + 
			"}";
		}
}
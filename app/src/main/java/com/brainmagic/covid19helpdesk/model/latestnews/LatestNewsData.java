package com.brainmagic.covid19helpdesk.model.latestnews;

import com.google.gson.annotations.SerializedName;

public class LatestNewsData {

	@SerializedName("id")
	private int id;

	@SerializedName("LatestNewslink")
	private String latestNewslink;

	@SerializedName("Topic")
	private String topic;

	@SerializedName("Date")
	private String date;

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setLatestNewslink(String latestNewslink){
		this.latestNewslink = latestNewslink;
	}

	public String getLatestNewslink(){
		return latestNewslink;
	}

	public void setTopic(String topic){
		this.topic = topic;
	}

	public String getTopic(){
		return topic;
	}

	public void setDate(String date){
		this.date = date;
	}

	public String getDate(){
		return date;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"id = '" + id + '\'' + 
			",latestNewslink = '" + latestNewslink + '\'' + 
			",topic = '" + topic + '\'' + 
			",date = '" + date + '\'' + 
			"}";
		}
}
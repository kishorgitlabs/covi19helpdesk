package com.brainmagic.covid19helpdesk.model.needy;

import com.google.gson.annotations.SerializedName;

public class NeedyDropDown{

	@SerializedName("result")
	private String result;

	@SerializedName("data")
	private NeedyData data;

	public void setResult(String result){
		this.result = result;
	}

	public String getResult(){
		return result;
	}

	public void setData(NeedyData data){
		this.data = data;
	}

	public NeedyData getData(){
		return data;
	}

	@Override
 	public String toString(){
		return 
			"NeedyDropDown{" + 
			"result = '" + result + '\'' + 
			",data = '" + data + '\'' + 
			"}";
		}
}
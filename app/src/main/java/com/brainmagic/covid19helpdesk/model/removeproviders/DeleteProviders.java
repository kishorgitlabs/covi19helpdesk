package com.brainmagic.covid19helpdesk.model.removeproviders;

import com.google.gson.annotations.SerializedName;

public class DeleteProviders{

	@SerializedName("result")
	private String result;

	@SerializedName("data")
	private Object data;

	public void setResult(String result){
		this.result = result;
	}

	public String getResult(){
		return result;
	}

	public void setData(Object data){
		this.data = data;
	}

	public Object getData(){
		return data;
	}

	@Override
 	public String toString(){
		return 
			"DeleteProviders{" + 
			"result = '" + result + '\'' + 
			",data = '" + data + '\'' + 
			"}";
		}
}
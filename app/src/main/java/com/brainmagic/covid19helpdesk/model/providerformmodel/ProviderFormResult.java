
package com.brainmagic.covid19helpdesk.model.providerformmodel;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ProviderFormResult {

    @SerializedName("ApprovedStatus")
    private Object mApprovedStatus;
    @SerializedName("Closetime")
    private String mClosetime;
    @SerializedName("contribute")
    private String mContribute;
    @SerializedName("contributeList")
    private Object mContributeList;
    @SerializedName("contributethrough")
    private String mContributethrough;
    @SerializedName("contributethroughList")
    private Object mContributethroughList;
    @SerializedName("Date")
    private String mDate;
    @SerializedName("Designation")
    private String mDesignation;
    @SerializedName("Donateasmoney")
    private String mDonateasmoney;
    @SerializedName("EmergencyList")
    private Object mEmergencyList;
    @SerializedName("emergencyuse")
    private String mEmergencyuse;
    @SerializedName("Gettouchwithme")
    private String mGettouchwithme;
    @SerializedName("Humanity")
    private String mHumanity;
    @SerializedName("id")
    private Long mId;
    @SerializedName("LocationContribute")
    private String mLocationContribute;
    @SerializedName("Manufacture")
    private String mManufacture;
    @SerializedName("ManufactureList")
    private Object mManufactureList;
    @SerializedName("NameofOrganization")
    private String mNameofOrganization;
    @SerializedName("Occupation")
    private String mOccupation;
    @SerializedName("OccupationList")
    private Object mOccupationList;
    @SerializedName("Opentime")
    private String mOpentime;
    @SerializedName("provide")
    private String mProvide;
    @SerializedName("ProvideList")
    private Object mProvideList;
    @SerializedName("Quantity")
    private String mQuantity;
    @SerializedName("RegId")
    private String mRegId;
    @SerializedName("Remark")
    private String mRemark;
    @SerializedName("shareyourpersonaldata")
    private Object mShareyourpersonaldata;
    @SerializedName("ShopLocation")
    private String mShopLocation;
    @SerializedName("supplymaterial")
    private String mSupplymaterial;
    @SerializedName("TypeOfBisList")
    private Object mTypeOfBisList;
    @SerializedName("TypeofBusiness")
    private String mTypeofBusiness;
    @SerializedName("UserType")
    private String mUserType;

    public Object getApprovedStatus() {
        return mApprovedStatus;
    }

    public void setApprovedStatus(Object approvedStatus) {
        mApprovedStatus = approvedStatus;
    }

    public String getClosetime() {
        return mClosetime;
    }

    public void setClosetime(String closetime) {
        mClosetime = closetime;
    }

    public String getContribute() {
        return mContribute;
    }

    public void setContribute(String contribute) {
        mContribute = contribute;
    }

    public Object getContributeList() {
        return mContributeList;
    }

    public void setContributeList(Object contributeList) {
        mContributeList = contributeList;
    }

    public String getContributethrough() {
        return mContributethrough;
    }

    public void setContributethrough(String contributethrough) {
        mContributethrough = contributethrough;
    }

    public Object getContributethroughList() {
        return mContributethroughList;
    }

    public void setContributethroughList(Object contributethroughList) {
        mContributethroughList = contributethroughList;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public String getDesignation() {
        return mDesignation;
    }

    public void setDesignation(String designation) {
        mDesignation = designation;
    }

    public String getDonateasmoney() {
        return mDonateasmoney;
    }

    public void setDonateasmoney(String donateasmoney) {
        mDonateasmoney = donateasmoney;
    }

    public Object getEmergencyList() {
        return mEmergencyList;
    }

    public void setEmergencyList(Object emergencyList) {
        mEmergencyList = emergencyList;
    }

    public String getEmergencyuse() {
        return mEmergencyuse;
    }

    public void setEmergencyuse(String emergencyuse) {
        mEmergencyuse = emergencyuse;
    }

    public String getGettouchwithme() {
        return mGettouchwithme;
    }

    public void setGettouchwithme(String gettouchwithme) {
        mGettouchwithme = gettouchwithme;
    }

    public String getHumanity() {
        return mHumanity;
    }

    public void setHumanity(String humanity) {
        mHumanity = humanity;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getLocationContribute() {
        return mLocationContribute;
    }

    public void setLocationContribute(String locationContribute) {
        mLocationContribute = locationContribute;
    }

    public String getManufacture() {
        return mManufacture;
    }

    public void setManufacture(String manufacture) {
        mManufacture = manufacture;
    }

    public Object getManufactureList() {
        return mManufactureList;
    }

    public void setManufactureList(Object manufactureList) {
        mManufactureList = manufactureList;
    }

    public String getNameofOrganization() {
        return mNameofOrganization;
    }

    public void setNameofOrganization(String nameofOrganization) {
        mNameofOrganization = nameofOrganization;
    }

    public String getOccupation() {
        return mOccupation;
    }

    public void setOccupation(String occupation) {
        mOccupation = occupation;
    }

    public Object getOccupationList() {
        return mOccupationList;
    }

    public void setOccupationList(Object occupationList) {
        mOccupationList = occupationList;
    }

    public String getOpentime() {
        return mOpentime;
    }

    public void setOpentime(String opentime) {
        mOpentime = opentime;
    }

    public String getProvide() {
        return mProvide;
    }

    public void setProvide(String provide) {
        mProvide = provide;
    }

    public Object getProvideList() {
        return mProvideList;
    }

    public void setProvideList(Object provideList) {
        mProvideList = provideList;
    }

    public String getQuantity() {
        return mQuantity;
    }

    public void setQuantity(String quantity) {
        mQuantity = quantity;
    }

    public String getRegId() {
        return mRegId;
    }

    public void setRegId(String regId) {
        mRegId = regId;
    }

    public String getRemark() {
        return mRemark;
    }

    public void setRemark(String remark) {
        mRemark = remark;
    }

    public Object getShareyourpersonaldata() {
        return mShareyourpersonaldata;
    }

    public void setShareyourpersonaldata(Object shareyourpersonaldata) {
        mShareyourpersonaldata = shareyourpersonaldata;
    }

    public String getShopLocation() {
        return mShopLocation;
    }

    public void setShopLocation(String shopLocation) {
        mShopLocation = shopLocation;
    }

    public String getSupplymaterial() {
        return mSupplymaterial;
    }

    public void setSupplymaterial(String supplymaterial) {
        mSupplymaterial = supplymaterial;
    }

    public Object getTypeOfBisList() {
        return mTypeOfBisList;
    }

    public void setTypeOfBisList(Object typeOfBisList) {
        mTypeOfBisList = typeOfBisList;
    }

    public String getTypeofBusiness() {
        return mTypeofBusiness;
    }

    public void setTypeofBusiness(String typeofBusiness) {
        mTypeofBusiness = typeofBusiness;
    }

    public String getUserType() {
        return mUserType;
    }

    public void setUserType(String userType) {
        mUserType = userType;
    }

}

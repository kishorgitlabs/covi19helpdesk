package com.brainmagic.covid19helpdesk.model.versioncheck;

import com.google.gson.annotations.SerializedName;

public class VersionCheck {

	@SerializedName("result")
	private String result;

	@SerializedName("data")
	private String data;

	public void setResult(String result){
		this.result = result;
	}

	public String getResult(){
		return result;
	}

	public void setData(String data){
		this.data = data;
	}

	public String getData(){
		return data;
	}

	@Override
 	public String toString(){
		return 
			"VersionCheck{" + 
			"result = '" + result + '\'' + 
			",data = '" + data + '\'' + 
			"}";
		}
}
package com.brainmagic.covid19helpdesk.model.needyformresult;

import com.google.gson.annotations.SerializedName;

public class NeedyFormData {

	@SerializedName("MobileNo")
	private String mobileNo;

	@SerializedName("ApproveStatus")
	private String approveStatus;

	@SerializedName("UpdateStatus")
	private String updateStatus;

	@SerializedName("Address")
	private String address;

	@SerializedName("Ineed")
	private String ineed;

	@SerializedName("LandMark")
	private String landMark;

	@SerializedName("Quantity")
	private String quantity;

	@SerializedName("StayingAlone")
	private String stayingAlone;

	@SerializedName("Gender")
	private String gender;

	@SerializedName("HealthConditionList")
	private Object healthConditionList;

	@SerializedName("INeedList")
	private Object iNeedList;

	@SerializedName("Date")
	private String date;

	@SerializedName("Name")
	private String name;

	@SerializedName("Remark")
	private String remark;

	@SerializedName("HowManyPeople")
	private String howManyPeople;

	@SerializedName("MedicineName")
	private Object medicineName;

	@SerializedName("id")
	private int id;

	@SerializedName("Regid")
	private String regid;

	@SerializedName("UpdateRemark")
	private Object updateRemark;

	@SerializedName("Age")
	private Object age;

	@SerializedName("MartialStatus")
	private String martialStatus;

	@SerializedName("HealthCondition")
	private String healthCondition;

	public void setMobileNo(String mobileNo){
		this.mobileNo = mobileNo;
	}

	public String getMobileNo(){
		return mobileNo;
	}

	public void setApproveStatus(String approveStatus){
		this.approveStatus = approveStatus;
	}

	public String getApproveStatus(){
		return approveStatus;
	}

	public void setUpdateStatus(String updateStatus){
		this.updateStatus = updateStatus;
	}

	public String getUpdateStatus(){
		return updateStatus;
	}

	public void setAddress(String address){
		this.address = address;
	}

	public String getAddress(){
		return address;
	}

	public void setIneed(String ineed){
		this.ineed = ineed;
	}

	public String getIneed(){
		return ineed;
	}

	public void setLandMark(String landMark){
		this.landMark = landMark;
	}

	public String getLandMark(){
		return landMark;
	}

	public void setQuantity(String quantity){
		this.quantity = quantity;
	}

	public String getQuantity(){
		return quantity;
	}

	public void setStayingAlone(String stayingAlone){
		this.stayingAlone = stayingAlone;
	}

	public String getStayingAlone(){
		return stayingAlone;
	}

	public void setGender(String gender){
		this.gender = gender;
	}

	public String getGender(){
		return gender;
	}

	public void setHealthConditionList(Object healthConditionList){
		this.healthConditionList = healthConditionList;
	}

	public Object getHealthConditionList(){
		return healthConditionList;
	}

	public void setINeedList(Object iNeedList){
		this.iNeedList = iNeedList;
	}

	public Object getINeedList(){
		return iNeedList;
	}

	public void setDate(String date){
		this.date = date;
	}

	public String getDate(){
		return date;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setRemark(String remark){
		this.remark = remark;
	}

	public String getRemark(){
		return remark;
	}

	public void setHowManyPeople(String howManyPeople){
		this.howManyPeople = howManyPeople;
	}

	public String getHowManyPeople(){
		return howManyPeople;
	}

	public void setMedicineName(Object medicineName){
		this.medicineName = medicineName;
	}

	public Object getMedicineName(){
		return medicineName;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setRegid(String regid){
		this.regid = regid;
	}

	public String getRegid(){
		return regid;
	}

	public void setUpdateRemark(Object updateRemark){
		this.updateRemark = updateRemark;
	}

	public Object getUpdateRemark(){
		return updateRemark;
	}

	public void setAge(Object age){
		this.age = age;
	}

	public Object getAge(){
		return age;
	}

	public void setMartialStatus(String martialStatus){
		this.martialStatus = martialStatus;
	}

	public String getMartialStatus(){
		return martialStatus;
	}

	public void setHealthCondition(String healthCondition){
		this.healthCondition = healthCondition;
	}

	public String getHealthCondition(){
		return healthCondition;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"mobileNo = '" + mobileNo + '\'' + 
			",approveStatus = '" + approveStatus + '\'' + 
			",updateStatus = '" + updateStatus + '\'' + 
			",address = '" + address + '\'' + 
			",ineed = '" + ineed + '\'' + 
			",landMark = '" + landMark + '\'' + 
			",quantity = '" + quantity + '\'' + 
			",stayingAlone = '" + stayingAlone + '\'' + 
			",gender = '" + gender + '\'' + 
			",healthConditionList = '" + healthConditionList + '\'' + 
			",iNeedList = '" + iNeedList + '\'' + 
			",date = '" + date + '\'' + 
			",name = '" + name + '\'' + 
			",remark = '" + remark + '\'' + 
			",howManyPeople = '" + howManyPeople + '\'' + 
			",medicineName = '" + medicineName + '\'' + 
			",id = '" + id + '\'' + 
			",regid = '" + regid + '\'' + 
			",updateRemark = '" + updateRemark + '\'' + 
			",age = '" + age + '\'' + 
			",martialStatus = '" + martialStatus + '\'' + 
			",healthCondition = '" + healthCondition + '\'' + 
			"}";
		}
}
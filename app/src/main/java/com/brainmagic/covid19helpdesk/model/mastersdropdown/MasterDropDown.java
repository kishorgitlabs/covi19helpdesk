package com.brainmagic.covid19helpdesk.model.mastersdropdown;

import com.google.gson.annotations.SerializedName;

public class MasterDropDown{

	@SerializedName("result")
	private String result;

	@SerializedName("data")
	private DropDownData data;

	public void setResult(String result){
		this.result = result;
	}

	public String getResult(){
		return result;
	}

	public void setData(DropDownData data){
		this.data = data;
	}

	public DropDownData getData(){
		return data;
	}

	@Override
 	public String toString(){
		return 
			"MasterDropDown{" + 
			"result = '" + result + '\'' + 
			",data = '" + data + '\'' + 
			"}";
		}
}
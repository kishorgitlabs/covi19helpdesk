
package com.brainmagic.covid19helpdesk.model.updateneeds;

import com.google.gson.annotations.SerializedName;

public class UpdateNeedyModel {

    @SerializedName("data")
    private UpdateNeedyResult mData;
    @SerializedName("result")
    private String mResult;

    public UpdateNeedyResult getData() {
        return mData;
    }

    public void setData(UpdateNeedyResult data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}

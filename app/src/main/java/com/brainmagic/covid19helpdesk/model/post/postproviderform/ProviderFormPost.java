package com.brainmagic.covid19helpdesk.model.post.postproviderform;

import com.brainmagic.covid19helpdesk.model.multiselect.ValueList;

import java.util.List;

public class ProviderFormPost  {

    String userType;
    String RegId;
    String ShopLocation;
    String Opentime;
    String Closetime;
    String NameofOrganization;
    String Designation;
    String Quantity;
    String LocationContribute;
    String Gettouchwithme;
    String Donateasmoney;
    String Humanity;
    String supplymaterial;
    String Remark;
    String shareyourpersonalData;
    List<ValueList> OccupationList;
    List<ValueList> TypeOfBisList;
    List<ValueList> contributeList;
    List<ValueList> contributethroughList;
    List<ValueList> ProvideList;
    List<ValueList> EmergencyList;
    List<ValueList> ManufactureList;

    public List<ValueList> getOccupationList() {
        return OccupationList;
    }

    public List<ValueList> getTypeOfBisList() {
        return TypeOfBisList;
    }

    public void setShareData(String shareData) {
        this.shareyourpersonalData = shareData;
    }

    public List<ValueList> getContributeList() {
        return contributeList;
    }

    public List<ValueList> getContributethroughList() {
        return contributethroughList;
    }

    public List<ValueList> getProvideList() {
        return ProvideList;
    }

    public List<ValueList> getEmergencyList() {
        return EmergencyList;
    }

    public List<ValueList> getManufactureList() {
        return ManufactureList;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public void setRegId(String regId) {
        RegId = regId;
    }

    public void setShopLocation(String shopLocation) {
        ShopLocation = shopLocation;
    }

    public void setOpentime(String opentime) {
        Opentime = opentime;
    }

    public void setClosetime(String closetime) {
        Closetime = closetime;
    }

    public void setNameofOrganization(String nameofOrganization) {
        NameofOrganization = nameofOrganization;
    }

    public void setDesignation(String designation) {
        Designation = designation;
    }

    public void setQuantity(String quantity) {
        Quantity = quantity;
    }

    public void setLocationContribute(String locationContribute) {
        LocationContribute = locationContribute;
    }

    public void setGettouchwithme(String gettouchwithme) {
        Gettouchwithme = gettouchwithme;
    }

    public void setDonateasmoney(String donateasmoney) {
        Donateasmoney = donateasmoney;
    }

    public void setHumanity(String humanity) {
        Humanity = humanity;
    }

    public void setSupplymaterial(String supplymaterial) {
        this.supplymaterial = supplymaterial;
    }

    public void setRemark(String remark) {
        Remark = remark;
    }

    public void setOccupationList(List<ValueList> occupationList) {
        OccupationList = occupationList;
    }

    public void setTypeOfBisList(List<ValueList> typeOfBisList) {
        TypeOfBisList = typeOfBisList;
    }

    public void setContributeList(List<ValueList> contributeList) {
        this.contributeList = contributeList;
    }

    public void setContributethroughList(List<ValueList> contributethroughList) {
        this.contributethroughList = contributethroughList;
    }

    public void setProvideList(List<ValueList> provideList) {
        ProvideList = provideList;
    }

    public void setEmergencyList(List<ValueList> emergencyList) {
        EmergencyList = emergencyList;
    }

    public void setManufactureList(List<ValueList> manufactureList) {
        ManufactureList = manufactureList;
    }
}

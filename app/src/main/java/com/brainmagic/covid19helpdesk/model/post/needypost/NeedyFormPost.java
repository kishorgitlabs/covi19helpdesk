package com.brainmagic.covid19helpdesk.model.post.needypost;

import com.brainmagic.covid19helpdesk.model.multiselect.ValueList;

import java.util.List;

public class NeedyFormPost {

    String RegId;
    String Name;
    String Gender;
    String MartialStatus;
    String MobileNo;
    String HowManyPeople;
    String StayingAlone;
    String Address;
    String LandMark;
    String Quantity;
    String Remark;
    String MedicineName;
    List<ValueList> HealthConditionList;
    List<ValueList> INeedList;

    public String getMedicineName() {
        return MedicineName;
    }

    public void setMedicineName(String medicineName) {
        MedicineName = medicineName;
    }



    public String getRegId() {
        return RegId;
    }

    public void setRegId(String regId) {
        RegId = regId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getMartialStatus() {
        return MartialStatus;
    }

    public void setMartialStatus(String martialStatus) {
        MartialStatus = martialStatus;
    }

    public String getMobileNo() {
        return MobileNo;
    }

    public void setMobileNo(String mobileNo) {
        MobileNo = mobileNo;
    }

    public String getHowManyPeople() {
        return HowManyPeople;
    }

    public void setHowManyPeople(String howManyPeople) {
        HowManyPeople = howManyPeople;
    }

    public String getStayingAlone() {
        return StayingAlone;
    }

    public void setStayingAlone(String stayingAlone) {
        StayingAlone = stayingAlone;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getLandMark() {
        return LandMark;
    }

    public void setLandMark(String landMark) {
        LandMark = landMark;
    }

    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String quantity) {
        Quantity = quantity;
    }

    public String getRemark() {
        return Remark;
    }

    public void setRemark(String remark) {
        Remark = remark;
    }

    public List<ValueList> getHealthConditionList() {
        return HealthConditionList;
    }

    public void setHealthConditionList(List<ValueList> healthConditionList) {
        HealthConditionList = healthConditionList;
    }

    public List<ValueList> getINeedList() {
        return INeedList;
    }

    public void setINeedList(List<ValueList> INeedList) {
        this.INeedList = INeedList;
    }


}



package com.brainmagic.covid19helpdesk.model.viewproviders;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ViewProviders{

	@SerializedName("result")
	private String result;

	@SerializedName("data")
	private List<ViewProvidersData> data;

	public void setResult(String result){
		this.result = result;
	}

	public String getResult(){
		return result;
	}

	public void setData(List<ViewProvidersData> data){
		this.data = data;
	}

	public List<ViewProvidersData> getData(){
		return data;
	}

	@Override
 	public String toString(){
		return 
			"ViewProviders{" + 
			"result = '" + result + '\'' + 
			",data = '" + data + '\'' + 
			"}";
		}
}
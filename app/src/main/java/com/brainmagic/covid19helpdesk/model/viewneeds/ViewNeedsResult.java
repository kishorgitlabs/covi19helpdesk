
package com.brainmagic.covid19helpdesk.model.viewneeds;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ViewNeedsResult {

    @SerializedName("Address")
    private String mAddress;
    @SerializedName("Age")
    private Object mAge;
    @SerializedName("ApproveStatus")
    private String mApproveStatus;
    @SerializedName("Date")
    private String mDate;
    @SerializedName("DeleteFlag")
    private String mDeleteFlag;
    @SerializedName("Gender")
    private String mGender;
    @SerializedName("HealthCondition")
    private String mHealthCondition;
    @SerializedName("HealthConditionList")
    private Object mHealthConditionList;
    @SerializedName("HowManyPeople")
    private String mHowManyPeople;
    @SerializedName("INeedList")
    private Object mINeedList;
    @SerializedName("id")
    private String mId;
    @SerializedName("Ineed")
    private String mIneed;
    @SerializedName("LandMark")
    private String mLandMark;
    @SerializedName("MartialStatus")
    private String mMartialStatus;
    @SerializedName("MedicineName")
    private String mMedicineName;
    @SerializedName("MobileNo")
    private String mMobileNo;
    @SerializedName("Name")
    private String mName;
    @SerializedName("Quantity")
    private String mQuantity;
    @SerializedName("Regid")
    private String mRegid;
    @SerializedName("Remark")
    private String mRemark;
    @SerializedName("StayingAlone")
    private String mStayingAlone;
    @SerializedName("UpdateRemark")
    private Object mUpdateRemark;
    @SerializedName("UpdateStatus")
    private String mUpdateStatus;

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String address) {
        mAddress = address;
    }

    public Object getAge() {
        return mAge;
    }

    public void setAge(Object age) {
        mAge = age;
    }

    public String getApproveStatus() {
        return mApproveStatus;
    }

    public void setApproveStatus(String approveStatus) {
        mApproveStatus = approveStatus;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public String getDeleteFlag() {
        return mDeleteFlag;
    }

    public void setDeleteFlag(String deleteFlag) {
        mDeleteFlag = deleteFlag;
    }

    public String getGender() {
        return mGender;
    }

    public void setGender(String gender) {
        mGender = gender;
    }

    public String getHealthCondition() {
        return mHealthCondition;
    }

    public void setHealthCondition(String healthCondition) {
        mHealthCondition = healthCondition;
    }

    public Object getHealthConditionList() {
        return mHealthConditionList;
    }

    public void setHealthConditionList(Object healthConditionList) {
        mHealthConditionList = healthConditionList;
    }

    public String getHowManyPeople() {
        return mHowManyPeople;
    }

    public void setHowManyPeople(String howManyPeople) {
        mHowManyPeople = howManyPeople;
    }

    public Object getINeedList() {
        return mINeedList;
    }

    public void setINeedList(Object iNeedList) {
        mINeedList = iNeedList;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getIneed() {
        return mIneed;
    }

    public void setIneed(String ineed) {
        mIneed = ineed;
    }

    public String getLandMark() {
        return mLandMark;
    }

    public void setLandMark(String landMark) {
        mLandMark = landMark;
    }

    public String getMartialStatus() {
        return mMartialStatus;
    }

    public void setMartialStatus(String martialStatus) {
        mMartialStatus = martialStatus;
    }

    public String getMedicineName() {
        return mMedicineName;
    }

    public void setMedicineName(String medicineName) {
        mMedicineName = medicineName;
    }

    public String getMobileNo() {
        return mMobileNo;
    }

    public void setMobileNo(String mobileNo) {
        mMobileNo = mobileNo;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getQuantity() {
        return mQuantity;
    }

    public void setQuantity(String quantity) {
        mQuantity = quantity;
    }

    public String getRegid() {
        return mRegid;
    }

    public void setRegid(String regid) {
        mRegid = regid;
    }

    public String getRemark() {
        return mRemark;
    }

    public void setRemark(String remark) {
        mRemark = remark;
    }

    public String getStayingAlone() {
        return mStayingAlone;
    }

    public void setStayingAlone(String stayingAlone) {
        mStayingAlone = stayingAlone;
    }

    public Object getUpdateRemark() {
        return mUpdateRemark;
    }

    public void setUpdateRemark(Object updateRemark) {
        mUpdateRemark = updateRemark;
    }

    public String getUpdateStatus() {
        return mUpdateStatus;
    }

    public void setUpdateStatus(String updateStatus) {
        mUpdateStatus = updateStatus;
    }

}

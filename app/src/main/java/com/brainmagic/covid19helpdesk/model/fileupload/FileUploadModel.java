
package com.brainmagic.covid19helpdesk.model.fileupload;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class FileUploadModel {

    @SerializedName("Message")
    private String mMessage;

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        mMessage = message;
    }

}

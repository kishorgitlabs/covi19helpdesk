package com.brainmagic.covid19helpdesk.model.mastersdropdown;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class DropDownData {

	@SerializedName("contributethrough")
	private List<String> contributethrough;

	@SerializedName("Occupation")
	private List<String> occupation;

	@SerializedName("contribute")
	private List<String> contribute;

	@SerializedName("emergency")
	private List<String> emergency;

	@SerializedName("typeofBusiness")
	private List<String> typeofBusiness;

	@SerializedName("userType")
	private List<String> userType;

	@SerializedName("Manufacture")
	private List<String> manufacture;

	@SerializedName("Provider")
	private List<String> provider;

	public void setContributethrough(List<String> contributethrough){
		this.contributethrough = contributethrough;
	}

	public List<String> getContributethrough(){
		return contributethrough;
	}

	public void setOccupation(List<String> occupation){
		this.occupation = occupation;
	}

	public List<String> getOccupation(){
		return occupation;
	}

	public void setContribute(List<String> contribute){
		this.contribute = contribute;
	}

	public List<String> getContribute(){
		return contribute;
	}

	public void setEmergency(List<String> emergency){
		this.emergency = emergency;
	}

	public List<String> getEmergency(){
		return emergency;
	}

	public void setTypeofBusiness(List<String> typeofBusiness){
		this.typeofBusiness = typeofBusiness;
	}

	public List<String> getTypeofBusiness(){
		return typeofBusiness;
	}

	public void setUserType(List<String> userType){
		this.userType = userType;
	}

	public List<String> getUserType(){
		return userType;
	}

	public void setManufacture(List<String> manufacture){
		this.manufacture = manufacture;
	}

	public List<String> getManufacture(){
		return manufacture;
	}

	public void setProvider(List<String> provider){
		this.provider = provider;
	}

	public List<String> getProvider(){
		return provider;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"contributethrough = '" + contributethrough + '\'' + 
			",occupation = '" + occupation + '\'' + 
			",contribute = '" + contribute + '\'' + 
			",emergency = '" + emergency + '\'' + 
			",typeofBusiness = '" + typeofBusiness + '\'' + 
			",userType = '" + userType + '\'' + 
			",manufacture = '" + manufacture + '\'' + 
			",provider = '" + provider + '\'' + 
			"}";
		}
}
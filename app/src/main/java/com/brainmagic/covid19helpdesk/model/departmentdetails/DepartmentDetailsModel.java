
package com.brainmagic.covid19helpdesk.model.departmentdetails;

import java.util.List;
import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class DepartmentDetailsModel {

    @SerializedName("data")
    private List<DepartmentDetailsResult> mData;
    @SerializedName("result")
    private String mResult;

    public List<DepartmentDetailsResult> getData() {
        return mData;
    }

    public void setData(List<DepartmentDetailsResult> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}

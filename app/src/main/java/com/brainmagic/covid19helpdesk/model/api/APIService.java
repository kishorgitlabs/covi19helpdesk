package com.brainmagic.covid19helpdesk.model.api;

import com.brainmagic.covid19helpdesk.model.StringListModel;
import com.brainmagic.covid19helpdesk.model.StringModel;
import com.brainmagic.covid19helpdesk.model.departmentdetails.DepartmentDetailsModel;
import com.brainmagic.covid19helpdesk.model.fileupload.FileUploadModel;
import com.brainmagic.covid19helpdesk.model.latestnews.LatestNews;
import com.brainmagic.covid19helpdesk.model.mastersdropdown.MasterDropDown;
import com.brainmagic.covid19helpdesk.model.needy.NeedyDropDown;
import com.brainmagic.covid19helpdesk.model.needyformresult.NeedyFormResult;
import com.brainmagic.covid19helpdesk.model.post.needypost.NeedyFormPost;
import com.brainmagic.covid19helpdesk.model.post.postproviderform.ProviderFormPost;
import com.brainmagic.covid19helpdesk.model.providerformmodel.ProviderFormModel;
import com.brainmagic.covid19helpdesk.model.registration.RegistrationModel;
import com.brainmagic.covid19helpdesk.model.removeproviders.DeleteProviders;
import com.brainmagic.covid19helpdesk.model.updateneeds.UpdateNeedyModel;
import com.brainmagic.covid19helpdesk.model.usefulllinks.UseFullLinksResult;
import com.brainmagic.covid19helpdesk.model.versioncheck.VersionCheck;
import com.brainmagic.covid19helpdesk.model.viewneeds.ViewNeedsModel;
import com.brainmagic.covid19helpdesk.model.viewproviders.ViewProviders;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface APIService {


    @FormUrlEncoded
    @POST("api/api/mobileregister")
    public Call<RegistrationModel> register (
            @Field("FirstName") String firstName,
            @Field("LastName") String lastName,
            @Field("MobileNo") String mobile,
            @Field("Email") String email,
            @Field("GpsAddress") String gpsAddress,
            @Field("CurrentAddress") String currentAddress,
            @Field("DoorNoPlatNo") String doorNO,
            @Field("BilldingAprtNo") String buildingNo,
            @Field("City") String city,
            @Field("Pincode") String pinCode,
            @Field("State") String state,
            @Field("Street") String street,
            @Field("LandMark") String landMark,
            @Field("UserType") String userType,
            @Field("Image") String image,
            @Field("OTP") String otp,
            @Field("Aadharcard") String aadharCard
    );

    @FormUrlEncoded
    @POST("api/api/ResendSendOTP")
    public Call<StringModel> resendOtp (
            @Field("MobileNo") String mobile
    );

    @FormUrlEncoded
    @POST("api/api/ValidateOTP")
    public Call<RegistrationModel> validateOtp (
            @Field("MobileNo") String mobile,
            @Field("otp") String otp
    );

    @Multipart
    @POST("api/Upload/PostUserImage")
    public Call<FileUploadModel> fileUpload(
            @Part MultipartBody.Part file
    );

    @FormUrlEncoded
    @POST("api/api/IndentifyPhoto")
    public Call<RegistrationModel> imageNameUpload(
            @Field("Regid") String regId,
            @Field("id") String id,
            @Field("Image") String image
    );

    @FormUrlEncoded
    @POST("api/api/IndentifyAadhar")
    public Call<RegistrationModel> fileNameUpload(
            @Field("Regid") String regId,
            @Field("id") String id,
            @Field("Aadharcard") String image
    );

    @FormUrlEncoded
    @POST("api/api/CheckMethod")
    public Call<RegistrationModel> approvalAcknowledge(
            @Field("Regid") String regId,
            @Field("id") String id
    );

    @GET("api/API/UsefulLink")
    public Call<UseFullLinksResult> getUseFull();


    @GET("api/API/LatestNews")
    public Call<LatestNews> getLatestNews();

    @GET("api/API/Master")
    public Call<MasterDropDown> getMasterData();

    @POST("api/api/ProviderForm")
    public Call<ProviderFormModel> postProviderForm(
            @Body ProviderFormPost providerFormPost
    );

    @GET("api/API/NeedyMaster")
    Call<NeedyDropDown> getNeedyData(
    );

    @POST("api/API/NeedyForm")
    Call<NeedyFormResult> needyPost(
            @Body NeedyFormPost needyFormPost
    );

    @GET("api/API/CheckVersion")
    Call<VersionCheck> versionCheck(
            @Query("version") int version
    );

    @FormUrlEncoded
    @POST("api/API/viewneedy")
    Call<ViewNeedsModel> viewNeedy(
            @Field("RegId") String tokenId
    );


    @GET("api/api/GetViewNeed")
    Call<ViewNeedsModel> viewOtherNeeds();

    @GET("api/api/NeedyGetMethod")
    Call<StringListModel> needsList();


    @FormUrlEncoded
    @POST("api/api/UpdateNeedy")
    Call<UpdateNeedyModel> updateMyNeed(
            @Field("RegId") String tokenId,
            @Field("id") String id,
            @Field("UpdateRemark") String updateRemark
    );

    @GET("api/API/ViewProvider")
    Call<ViewProviders> getProviders(

    );

    @FormUrlEncoded
    @POST("api/API/ViewOurProvider")
    Call<ViewProviders> getProvidersDetails(
            @Field("RegId") String Regid
    );

    @FormUrlEncoded
    @POST("api/API/UpdateProvider")
    Call<DeleteProviders> deletProvides(
            @Field("RegId") String Regid,
            @Field("id")int id
    );


    @GET("api/api/GetState")
    Call<StringListModel> stateList();

    @FormUrlEncoded
    @POST("api/API/GetStateWiseDistrict")
    Call<StringListModel> cityList(
            @Field("State") String state
    );


    @GET("api/API/GetDepartment")
    Call<StringListModel> depList();

    @FormUrlEncoded
    @POST("api/API/GetDistrictWiseData1")
    Call<DepartmentDetailsModel> getWareHouseData(
            @Field("State") String state,
            @Field("District") String city,
            @Field("Department") String dep
    );

}

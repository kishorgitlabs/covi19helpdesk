
package com.brainmagic.covid19helpdesk.model.providerformmodel;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class ProviderFormModel {

    @SerializedName("data")
    private ProviderFormResult mData;
    @SerializedName("result")
    private String mResult;

    public ProviderFormResult getData() {
        return mData;
    }

    public void setData(ProviderFormResult data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}

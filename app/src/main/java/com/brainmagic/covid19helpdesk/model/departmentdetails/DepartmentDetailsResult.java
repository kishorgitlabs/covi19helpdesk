
package com.brainmagic.covid19helpdesk.model.departmentdetails;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class DepartmentDetailsResult {

    @SerializedName("Date")
    private String mDate;
    @SerializedName("Department")
    private String mDepartment;
    @SerializedName("District")
    private String mDistrict;
    @SerializedName("id")
    private Long mId;
    @SerializedName("MobileNo")
    private String mMobileNo;
    @SerializedName("NameOfPlace")
    private Object mNameOfPlace;
    @SerializedName("State")
    private String mState;

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public String getDepartment() {
        return mDepartment;
    }

    public void setDepartment(String department) {
        mDepartment = department;
    }

    public String getDistrict() {
        return mDistrict;
    }

    public void setDistrict(String district) {
        mDistrict = district;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getMobileNo() {
        return mMobileNo;
    }

    public void setMobileNo(String mobileNo) {
        mMobileNo = mobileNo;
    }

    public Object getNameOfPlace() {
        return mNameOfPlace;
    }

    public void setNameOfPlace(Object nameOfPlace) {
        mNameOfPlace = nameOfPlace;
    }

    public String getState() {
        return mState;
    }

    public void setState(String state) {
        mState = state;
    }

}

package com.brainmagic.covid19helpdesk.model.viewproviders;

import com.google.gson.annotations.SerializedName;

public class ViewProvidersData {

	@SerializedName("EmergencyList")
	private Object emergencyList;

	@SerializedName("contribute")
	private String contribute;

	@SerializedName("contributeList")
	private Object contributeList;

	@SerializedName("Remark")
	private String remark;

	@SerializedName("contributethrough")
	private String contributethrough;

	@SerializedName("TypeofBusiness")
	private Object typeofBusiness;

	@SerializedName("ProvideList")
	private Object provideList;

	@SerializedName("ShopLocation")
	private String shopLocation;

	@SerializedName("Humanity")
	private String humanity;

	@SerializedName("shareyourpersonaldata")
	private String shareyourpersonaldata;

	@SerializedName("id")
	private int id;

	@SerializedName("Closetime")
	private String closetime;

	@SerializedName("RegId")
	private String regId;

	@SerializedName("OccupationList")
	private Object occupationList;

	@SerializedName("LocationContribute")
	private Object locationContribute;

	@SerializedName("Designation")
	private String designation;

	@SerializedName("Opentime")
	private String opentime;

	@SerializedName("supplymaterial")
	private String supplymaterial;

	@SerializedName("ApprovedStatus")
	private String approvedStatus;

	@SerializedName("Quantity")
	private String quantity;

	@SerializedName("contributethroughList")
	private Object contributethroughList;

	@SerializedName("Donateasmoney")
	private String donateasmoney;

	@SerializedName("TypeOfBisList")
	private Object typeOfBisList;

	@SerializedName("emergencyuse")
	private String emergencyuse;

	@SerializedName("Date")
	private String date;

	@SerializedName("DeleteFlag")
	private String deleteFlag;

	@SerializedName("Occupation")
	private String occupation;

	@SerializedName("ManufactureList")
	private Object manufactureList;

	@SerializedName("provide")
	private String provide;

	@SerializedName("Gettouchwithme")
	private String gettouchwithme;

	@SerializedName("Manufacture")
	private String manufacture;

	@SerializedName("UserType")
	private String userType;

	@SerializedName("NameofOrganization")
	private String nameofOrganization;

	public void setEmergencyList(Object emergencyList){
		this.emergencyList = emergencyList;
	}

	public Object getEmergencyList(){
		return emergencyList;
	}

	public void setContribute(String contribute){
		this.contribute = contribute;
	}

	public String getContribute(){
		return contribute;
	}

	public void setContributeList(Object contributeList){
		this.contributeList = contributeList;
	}

	public Object getContributeList(){
		return contributeList;
	}

	public void setRemark(String remark){
		this.remark = remark;
	}

	public String getRemark(){
		return remark;
	}

	public void setContributethrough(String contributethrough){
		this.contributethrough = contributethrough;
	}

	public String getContributethrough(){
		return contributethrough;
	}

	public void setTypeofBusiness(Object typeofBusiness){
		this.typeofBusiness = typeofBusiness;
	}

	public Object getTypeofBusiness(){
		return typeofBusiness;
	}

	public void setProvideList(Object provideList){
		this.provideList = provideList;
	}

	public Object getProvideList(){
		return provideList;
	}

	public void setShopLocation(String shopLocation){
		this.shopLocation = shopLocation;
	}

	public String getShopLocation(){
		return shopLocation;
	}

	public void setHumanity(String humanity){
		this.humanity = humanity;
	}

	public String getHumanity(){
		return humanity;
	}

	public void setShareyourpersonaldata(String shareyourpersonaldata){
		this.shareyourpersonaldata = shareyourpersonaldata;
	}

	public String getShareyourpersonaldata(){
		return shareyourpersonaldata;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setClosetime(String closetime){
		this.closetime = closetime;
	}

	public String getClosetime(){
		return closetime;
	}

	public void setRegId(String regId){
		this.regId = regId;
	}

	public String getRegId(){
		return regId;
	}

	public void setOccupationList(Object occupationList){
		this.occupationList = occupationList;
	}

	public Object getOccupationList(){
		return occupationList;
	}

	public void setLocationContribute(Object locationContribute){
		this.locationContribute = locationContribute;
	}

	public Object getLocationContribute(){
		return locationContribute;
	}

	public void setDesignation(String designation){
		this.designation = designation;
	}

	public String getDesignation(){
		return designation;
	}

	public void setOpentime(String opentime){
		this.opentime = opentime;
	}

	public String getOpentime(){
		return opentime;
	}

	public void setSupplymaterial(String supplymaterial){
		this.supplymaterial = supplymaterial;
	}

	public String getSupplymaterial(){
		return supplymaterial;
	}

	public void setApprovedStatus(String approvedStatus){
		this.approvedStatus = approvedStatus;
	}

	public String getApprovedStatus(){
		return approvedStatus;
	}

	public void setQuantity(String quantity){
		this.quantity = quantity;
	}

	public String getQuantity(){
		return quantity;
	}

	public void setContributethroughList(Object contributethroughList){
		this.contributethroughList = contributethroughList;
	}

	public Object getContributethroughList(){
		return contributethroughList;
	}

	public void setDonateasmoney(String donateasmoney){
		this.donateasmoney = donateasmoney;
	}

	public String getDonateasmoney(){
		return donateasmoney;
	}

	public void setTypeOfBisList(Object typeOfBisList){
		this.typeOfBisList = typeOfBisList;
	}

	public Object getTypeOfBisList(){
		return typeOfBisList;
	}

	public void setEmergencyuse(String emergencyuse){
		this.emergencyuse = emergencyuse;
	}

	public String getEmergencyuse(){
		return emergencyuse;
	}

	public void setDate(String date){
		this.date = date;
	}

	public String getDate(){
		return date;
	}

	public void setDeleteFlag(String deleteFlag){
		this.deleteFlag = deleteFlag;
	}

	public String getDeleteFlag(){
		return deleteFlag;
	}

	public void setOccupation(String occupation){
		this.occupation = occupation;
	}

	public String getOccupation(){
		return occupation;
	}

	public void setManufactureList(Object manufactureList){
		this.manufactureList = manufactureList;
	}

	public Object getManufactureList(){
		return manufactureList;
	}

	public void setProvide(String provide){
		this.provide = provide;
	}

	public String getProvide(){
		return provide;
	}

	public void setGettouchwithme(String gettouchwithme){
		this.gettouchwithme = gettouchwithme;
	}

	public String getGettouchwithme(){
		return gettouchwithme;
	}

	public void setManufacture(String manufacture){
		this.manufacture = manufacture;
	}

	public String getManufacture(){
		return manufacture;
	}

	public void setUserType(String userType){
		this.userType = userType;
	}

	public String getUserType(){
		return userType;
	}

	public void setNameofOrganization(String nameofOrganization){
		this.nameofOrganization = nameofOrganization;
	}

	public String getNameofOrganization(){
		return nameofOrganization;
	}

	@Override
 	public String toString(){
		return 
			"DataItem{" + 
			"emergencyList = '" + emergencyList + '\'' + 
			",contribute = '" + contribute + '\'' + 
			",contributeList = '" + contributeList + '\'' + 
			",remark = '" + remark + '\'' + 
			",contributethrough = '" + contributethrough + '\'' + 
			",typeofBusiness = '" + typeofBusiness + '\'' + 
			",provideList = '" + provideList + '\'' + 
			",shopLocation = '" + shopLocation + '\'' + 
			",humanity = '" + humanity + '\'' + 
			",shareyourpersonaldata = '" + shareyourpersonaldata + '\'' + 
			",id = '" + id + '\'' + 
			",closetime = '" + closetime + '\'' + 
			",regId = '" + regId + '\'' + 
			",occupationList = '" + occupationList + '\'' + 
			",locationContribute = '" + locationContribute + '\'' + 
			",designation = '" + designation + '\'' + 
			",opentime = '" + opentime + '\'' + 
			",supplymaterial = '" + supplymaterial + '\'' + 
			",approvedStatus = '" + approvedStatus + '\'' + 
			",quantity = '" + quantity + '\'' + 
			",contributethroughList = '" + contributethroughList + '\'' + 
			",donateasmoney = '" + donateasmoney + '\'' + 
			",typeOfBisList = '" + typeOfBisList + '\'' + 
			",emergencyuse = '" + emergencyuse + '\'' + 
			",date = '" + date + '\'' + 
			",deleteFlag = '" + deleteFlag + '\'' + 
			",occupation = '" + occupation + '\'' + 
			",manufactureList = '" + manufactureList + '\'' + 
			",provide = '" + provide + '\'' + 
			",gettouchwithme = '" + gettouchwithme + '\'' + 
			",manufacture = '" + manufacture + '\'' + 
			",userType = '" + userType + '\'' + 
			",nameofOrganization = '" + nameofOrganization + '\'' + 
			"}";
		}
}
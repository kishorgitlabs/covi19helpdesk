package com.brainmagic.covid19helpdesk.model.needy;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class NeedyData {

	@SerializedName("INeedy")
	private List<String> iNeedy;

	@SerializedName("HealthCondition")
	private List<String> healthCondition;

	public void setINeedy(List<String> iNeedy){
		this.iNeedy = iNeedy;
	}

	public List<String> getINeedy(){
		return iNeedy;
	}

	public void setHealthCondition(List<String> healthCondition){
		this.healthCondition = healthCondition;
	}

	public List<String> getHealthCondition(){
		return healthCondition;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"iNeedy = '" + iNeedy + '\'' + 
			",healthCondition = '" + healthCondition + '\'' + 
			"}";
		}
}
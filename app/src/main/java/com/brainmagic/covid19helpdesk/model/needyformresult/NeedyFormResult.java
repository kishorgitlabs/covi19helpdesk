package com.brainmagic.covid19helpdesk.model.needyformresult;

import com.google.gson.annotations.SerializedName;

public class NeedyFormResult{

	@SerializedName("result")
	private String result;

	@SerializedName("data")
	private NeedyFormData data;

	public void setResult(String result){
		this.result = result;
	}

	public String getResult(){
		return result;
	}

	public void setData(NeedyFormData data){
		this.data = data;
	}

	public NeedyFormData getData(){
		return data;
	}

	@Override
 	public String toString(){
		return 
			"NeedyFormResult{" + 
			"result = '" + result + '\'' + 
			",data = '" + data + '\'' + 
			"}";
		}
}
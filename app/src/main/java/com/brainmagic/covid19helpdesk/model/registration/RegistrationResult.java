
package com.brainmagic.covid19helpdesk.model.registration;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class RegistrationResult {

    @SerializedName("Aadharcard")
    private String mAadharcard;
    @SerializedName("Approve")
    private String mApprove;
    @SerializedName("BilldingAprtNo")
    private String mBilldingAprtNo;
    @SerializedName("City")
    private String mCity;
    @SerializedName("CurrentAddress")
    private String mCurrentAddress;
    @SerializedName("Date")
    private String mDate;
    @SerializedName("DoorNoPlatNo")
    private String mDoorNoPlatNo;
    @SerializedName("Email")
    private String mEmail;
    @SerializedName("FirstName")
    private String mFirstName;
    @SerializedName("GpsAddress")
    private String mGpsAddress;
    @SerializedName("id")
    private int mId;
    @SerializedName("Image")
    private String mImage;
    @SerializedName("LandMark")
    private String mLandMark;
    @SerializedName("LastName")
    private String mLastName;
    @SerializedName("MobileNo")
    private String mMobileNo;
    @SerializedName("OTP")
    private String mOTP;
    @SerializedName("OTPStatus")
    private String mOTPStatus;
    @SerializedName("Pincode")
    private String mPincode;
    @SerializedName("State")
    private String mState;
    @SerializedName("Street")
    private String mStreet;
    @SerializedName("UserType")
    private String mUserType;
    @SerializedName("Regid")
    private String tokenId;

    public String getTokenId() {
        return tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    public String getAadharcard() {
        return mAadharcard;
    }

    public void setAadharcard(String aadharcard) {
        mAadharcard = aadharcard;
    }

    public String getApprove() {
        return mApprove;
    }

    public void setApprove(String approve) {
        mApprove = approve;
    }

    public String getBilldingAprtNo() {
        return mBilldingAprtNo;
    }

    public void setBilldingAprtNo(String billdingAprtNo) {
        mBilldingAprtNo = billdingAprtNo;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String city) {
        mCity = city;
    }

    public String getCurrentAddress() {
        return mCurrentAddress;
    }

    public void setCurrentAddress(String currentAddress) {
        mCurrentAddress = currentAddress;
    }

    public String getDate() {
        return mDate;
    }

    public void setDate(String date) {
        mDate = date;
    }

    public String getDoorNoPlatNo() {
        return mDoorNoPlatNo;
    }

    public void setDoorNoPlatNo(String doorNoPlatNo) {
        mDoorNoPlatNo = doorNoPlatNo;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public void setFirstName(String firstName) {
        mFirstName = firstName;
    }

    public String getGpsAddress() {
        return mGpsAddress;
    }

    public void setGpsAddress(String gpsAddress) {
        mGpsAddress = gpsAddress;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getImage() {
        return mImage;
    }

    public void setImage(String image) {
        mImage = image;
    }

    public String getLandMark() {
        return mLandMark;
    }

    public void setLandMark(String landMark) {
        mLandMark = landMark;
    }

    public String getLastName() {
        return mLastName;
    }

    public void setLastName(String lastName) {
        mLastName = lastName;
    }

    public String getMobileNo() {
        return mMobileNo;
    }

    public void setMobileNo(String mobileNo) {
        mMobileNo = mobileNo;
    }

    public String getOTP() {
        return mOTP;
    }

    public void setOTP(String oTP) {
        mOTP = oTP;
    }

    public String getOTPStatus() {
        return mOTPStatus;
    }

    public void setOTPStatus(String oTPStatus) {
        mOTPStatus = oTPStatus;
    }

    public String getPincode() {
        return mPincode;
    }

    public void setPincode(String pincode) {
        mPincode = pincode;
    }

    public String getState() {
        return mState;
    }

    public void setState(String state) {
        mState = state;
    }

    public String getStreet() {
        return mStreet;
    }

    public void setStreet(String street) {
        mStreet = street;
    }

    public String getUserType() {
        return mUserType;
    }

    public void setUserType(String userType) {
        mUserType = userType;
    }

}

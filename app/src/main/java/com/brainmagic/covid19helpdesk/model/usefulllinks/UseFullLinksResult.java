package com.brainmagic.covid19helpdesk.model.usefulllinks;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class UseFullLinksResult {

	@SerializedName("result")
	private String result;

	@SerializedName("data")
	private List<UseFullData> data;

	public void setResult(String result){
		this.result = result;
	}

	public String getResult(){
		return result;
	}

	public void setData(List<UseFullData> data){
		this.data = data;
	}

	public List<UseFullData> getData(){
		return data;
	}

	@Override
 	public String toString(){
		return 
			"UseFullLinks{" + 
			"result = '" + result + '\'' + 
			",data = '" + data + '\'' + 
			"}";
		}
}
package com.brainmagic.covid19helpdesk.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.brainmagic.covid19helpdesk.Constants;
import com.brainmagic.covid19helpdesk.R;
import com.brainmagic.covid19helpdesk.adapter.ThemeColorAdapter;
import com.brainmagic.covid19helpdesk.alert.AlertBox;
import com.brainmagic.covid19helpdesk.alert.TransparentProgressDialog;
import com.brainmagic.covid19helpdesk.model.api.APIService;
import com.brainmagic.covid19helpdesk.model.api.RetroClient;
import com.brainmagic.covid19helpdesk.model.mastersdropdown.DropDownData;
import com.brainmagic.covid19helpdesk.model.mastersdropdown.MasterDropDown;
import com.brainmagic.covid19helpdesk.model.multiselect.ValueList;
import com.brainmagic.covid19helpdesk.model.post.postproviderform.ProviderFormPost;
import com.brainmagic.covid19helpdesk.model.providerformmodel.ProviderFormModel;
import com.brainmagic.covid19helpdesk.network.NetworkConnection;
import com.brainmagic.covid19helpdesk.spinner.MultiSelectionSpinner;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProviderFragment extends Fragment {

    private DropDownData dropDownData;
    private MultiSelectionSpinner  businessType, toWhomContribute, iWantProvide, iHavingHall, ilikeManufacture,
    iContributeThrough;
    private Spinner userTypeSp, occupationSp, travelToSupply, volunteer, doShare, getTouch;
//    private EditText donateMoney, locationToContribute;
    private boolean occupationOther, occupationShop, isOtherOrg, toWhom, toProvide, toContribute;
    private ProviderFormPost providerFormPost;
    private SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);

        final View view = LayoutInflater.from(getActivity()).inflate(R.layout.provider_fragement, container, false);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        providerFormPost = new ProviderFormPost();
        sharedPref = getActivity().getSharedPreferences(Constants.sharedConfig, Context.MODE_PRIVATE);
        editor = sharedPref.edit();

        final LinearLayout individualLayout = view.findViewById(R.id.individual_user_layout);
        final LinearLayout organisationLayout = view.findViewById(R.id.organiser_layout);
        final LinearLayout individualOccLayout = view.findViewById(R.id.individual_occ_layout);
        userTypeSp = view.findViewById(R.id.user_type_sp);
        occupationSp = view.findViewById(R.id.occupation_sp);
        businessType = view.findViewById(R.id.business_type);
        toWhomContribute = view.findViewById(R.id.to_whom);
        iWantProvide = view.findViewById(R.id.i_want_provide);
        iHavingHall = view.findViewById(R.id.i_am_having_hall);
        ilikeManufacture = view.findViewById(R.id.i_like_manufacture);
        iContributeThrough = view.findViewById(R.id.i_contribute_through);
        travelToSupply = view.findViewById(R.id.travel_sp);
        volunteer = view.findViewById(R.id.volunteer_sp);
        doShare = view.findViewById(R.id.do_share_sp);
        getTouch = view.findViewById(R.id.get_touch_sp);
        TextView fragmenttitle=getActivity().findViewById(R.id.header);
        final EditText otherOccupationEt=view.findViewById(R.id.other_occupation_et);
        final EditText organisationNameEt=view.findViewById(R.id.organisation_name);
        final EditText orgDesignationEt=view.findViewById(R.id.org_designation);
        final EditText otherOrganisationEt=view.findViewById(R.id.others_org);
        final EditText otherToWhomEt=view.findViewById(R.id.others_to_whom);
        final EditText otherIProvideEt=view.findViewById(R.id.other_i_provide);
        final EditText otherQtyEt=view.findViewById(R.id.others_qty);
        final EditText otherContributeThroughEt=view.findViewById(R.id.others_contribute_through);
        final EditText shopLocationEt=view.findViewById(R.id.shop_location_et);
        final EditText openingTimeEt=view.findViewById(R.id.opening_time_et);
        final EditText closingTimeEt=view.findViewById(R.id.closing_time_et);
        final EditText whichLocationEt=view.findViewById(R.id.which_location_et);
        final EditText donateMoneyEt=view.findViewById(R.id.donate_money);
        final EditText remarksEt=view.findViewById(R.id.remarks);
        final Button submitBt=view.findViewById(R.id.submit_bt);
        fragmenttitle.setText(R.string.providerhead);

        occupationSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.i("TAG", "onItemSelected: ");
                String occupation = parent.getAdapter().getItem(position).toString();
                if(!"Occupation".equalsIgnoreCase(occupation))
                {
                   ValueList valueList = new ValueList();
                   valueList.setValue(occupation);
                   List<ValueList> lists = new ArrayList<>();
                   lists.add(valueList);
                   providerFormPost.setOccupationList(lists);
                }
                if(occupation.equals("Others"))
                {
                    otherOccupationEt.setVisibility(View.VISIBLE);
                }
                else {
                    otherOccupationEt.setVisibility(View.GONE);
                }
                if(occupation.equals("Super Market Owner")||occupation.equals("Milk Supply")||occupation.equals("Veg -Fruit Shop")||occupation.equals("Pharmacy Shop")){
                    individualOccLayout.setVisibility(View.VISIBLE);
                }
                else {
                    individualOccLayout.setVisibility(View.GONE);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        userTypeSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.i("TAG", "onItemSelected: ");
                String userType = parent.getAdapter().getItem(position).toString();
                if(userType.equalsIgnoreCase(getString(R.string.choose_user_type)))
                {
                    individualLayout.setVisibility(View.GONE);
                    organisationLayout.setVisibility(View.GONE);

                }
                else if(userType.equalsIgnoreCase(getString(R.string.individual_user)))
                {
                    individualLayout.setVisibility(View.VISIBLE);
                    organisationLayout.setVisibility(View.GONE);

                }
                else {
                    individualLayout.setVisibility(View.GONE);
                    organisationLayout.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


//        occupationMsp.setListener(new MultiSelectionSpinner.OnMultipleItemsSelectedListener() {
//            @Override
//            public void selectedIndices(List<Integer> indices) {
//
//            }
//
//            @Override
//            public void selectedStrings(List<ValueList> strings) {
//                providerFormPost.setOccupationList(strings);
//                if(occupationMsp.isOthers("Others"))
//                {
//                    otherOccupationEt.setVisibility(View.VISIBLE);
//                }
//                else {
//                    otherOccupationEt.setVisibility(View.GONE);
//                }
//                if(occupationMsp.isOthers("Super Market Owner","Milk Supply","Veg -Fruit Shop", "Pharmacy Shop")){
//                    individualOccLayout.setVisibility(View.VISIBLE);
//                }
//                else {
//                    individualOccLayout.setVisibility(View.GONE);
//                }
//
////                shopTypeLists=strings;
//            }
//
//        });

        businessType.setListener(new MultiSelectionSpinner.OnMultipleItemsSelectedListener() {
            @Override
            public void selectedIndices(List<Integer> indices) {

            }

            @Override
            public void selectedStrings(List<ValueList> strings) {
                providerFormPost.setTypeOfBisList(strings);
                if(businessType.isOthers("Others"))
                {
                    otherOrganisationEt.setVisibility(View.VISIBLE);

                }
                else {
                    otherOrganisationEt.setVisibility(View.GONE);
                }


//                shopTypeLists=strings;
            }

        });
        toWhomContribute.setListener(new MultiSelectionSpinner.OnMultipleItemsSelectedListener() {
            @Override
            public void selectedIndices(List<Integer> indices) {

            }

            @Override
            public void selectedStrings(List<ValueList> strings) {
                providerFormPost.setContributeList(strings);
                if(toWhomContribute.isOthers("Others"))
                {
                    otherToWhomEt.setVisibility(View.VISIBLE);

                }
                else {
                    otherToWhomEt.setVisibility(View.GONE);
                }


//                shopTypeLists=strings;
            }

        });
        iWantProvide.setListener(new MultiSelectionSpinner.OnMultipleItemsSelectedListener() {
            @Override
            public void selectedIndices(List<Integer> indices) {

            }

            @Override
            public void selectedStrings(List<ValueList> strings) {
                providerFormPost.setProvideList(strings);
                if(toWhomContribute.isOthers("Others"))
                {
                    otherIProvideEt.setVisibility(View.VISIBLE);

                }
                else {
                    otherIProvideEt.setVisibility(View.GONE);
                }


//                shopTypeLists=strings;
            }

        });
        iHavingHall.setListener(new MultiSelectionSpinner.OnMultipleItemsSelectedListener() {
            @Override
            public void selectedIndices(List<Integer> indices) {

            }

            @Override
            public void selectedStrings(List<ValueList> strings) {

                providerFormPost.setEmergencyList(strings);


//                shopTypeLists=strings;
            }

        });
        ilikeManufacture.setListener(new MultiSelectionSpinner.OnMultipleItemsSelectedListener() {
            @Override
            public void selectedIndices(List<Integer> indices) {

            }

            @Override
            public void selectedStrings(List<ValueList> strings) {
                providerFormPost.setManufactureList(strings);

//                shopTypeLists=strings;
            }

        });
        iContributeThrough.setListener(new MultiSelectionSpinner.OnMultipleItemsSelectedListener() {
            @Override
            public void selectedIndices(List<Integer> indices) {

            }

            @Override
            public void selectedStrings(List<ValueList> strings) {
                providerFormPost.setContributethroughList(strings);
                if(iContributeThrough.isOthers("Others"))
                {
                    otherContributeThroughEt.setVisibility(View.VISIBLE);

                }
                else {
                    otherContributeThroughEt.setVisibility(View.GONE);
                }
//                shopTypeLists=strings;
            }

        });

        List<String> stringList= new ArrayList<>();
        stringList.add("Choose");
        stringList.add("Yes");
        stringList.add("No");

        setDoShare(stringList);
        setGetTouch(stringList);
        setVolunteer(stringList);
        setTravelToSupply(stringList);

        checkInternet(false);

        submitBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userType = userTypeSp.getSelectedItem().toString();
                String shopLocation =shopLocationEt.getText().toString();
                String openTime =openingTimeEt.getText().toString();
                String closingTime =closingTimeEt.getText().toString();
                String organisationName =organisationNameEt.getText().toString();
                String orgDesignation =orgDesignationEt.getText().toString();
                String otherOccupationSt =otherOccupationEt.getText().toString();
                String otherBusinessSt =otherOrganisationEt.getText().toString();
                String otherContribute =otherToWhomEt.getText().toString();
                String otherProvide =otherIProvideEt.getText().toString();
                String otherQty =otherQtyEt.getText().toString();
                String otherContributeThrough =otherContributeThroughEt.getText().toString();
                String getTouchSt =getTouch.getSelectedItem().toString();
                String volunteerSt =volunteer.getSelectedItem().toString();
                String shareData =doShare.getSelectedItem().toString();
                String toTravel =travelToSupply.getSelectedItem().toString();
                String donateMoneySt =donateMoneyEt.getText().toString();
                String remarks =remarksEt.getText().toString();




                if(userType.equalsIgnoreCase(getString(R.string.choose_user_type)))
                {
                    Toast.makeText(getActivity(), getString(R.string.choose_user_type), Toast.LENGTH_SHORT).show();
                }
                else if(providerFormPost.getProvideList() == null)
                {
                    Toast.makeText(getActivity(), getString(R.string.choose_provide), Toast.LENGTH_SHORT).show();
                }
                else if(providerFormPost.getProvideList().size() == 0)
                {
                    Toast.makeText(getActivity(), getString(R.string.choose_provide), Toast.LENGTH_SHORT).show();
                }
                else {
                    if(getTouchSt.equalsIgnoreCase("Choose"))
                    {
                        getTouchSt = "";
                    }
                    if(volunteerSt.equalsIgnoreCase("Choose"))
                    {
                        volunteerSt = "";
                    }
                    if(shareData.equalsIgnoreCase("Choose"))
                    {
                        shareData = "";
                    }
                    if(toTravel.equalsIgnoreCase("Choose"))
                    {
                        toTravel = "";
                    }

                    if(!TextUtils.isEmpty(otherOccupationSt))
                    {
                        ValueList value = new ValueList();
                        value.setValue(otherOccupationSt);
                        providerFormPost.getOccupationList().add(value);
                    }
                    if(!TextUtils.isEmpty(otherBusinessSt)) {
                        ValueList value = new ValueList();
                        value.setValue(otherBusinessSt);
                        providerFormPost.getTypeOfBisList().add(value);
                    }
                    if(!TextUtils.isEmpty(otherContribute)) {
                        ValueList value = new ValueList();
                        value.setValue(otherContribute);
                        providerFormPost.getContributeList().add(value);
                    }
                    if(!TextUtils.isEmpty(otherProvide)) {
                        ValueList value = new ValueList();
                        value.setValue(otherProvide);
                        providerFormPost.getProvideList().add(value);
                    }
                    if(!TextUtils.isEmpty(otherProvide)) {
                        ValueList value = new ValueList();
                        value.setValue(otherContributeThrough);
                        providerFormPost.getContributethroughList().add(value);
                    }
                    providerFormPost.setShopLocation(shopLocation);
                    providerFormPost.setOpentime(openTime);
                    providerFormPost.setClosetime(closingTime);
                    providerFormPost.setNameofOrganization(organisationName);
                    providerFormPost.setDesignation(orgDesignation);
                    providerFormPost.setQuantity(otherQty);
                    providerFormPost.setGettouchwithme(getTouchSt);
                    providerFormPost.setHumanity(volunteerSt);
                    providerFormPost.setShareData(shareData);
                    providerFormPost.setDonateasmoney(donateMoneySt);
                    providerFormPost.setSupplymaterial(toTravel);
                    providerFormPost.setRemark(remarks);
                    providerFormPost.setRegId(sharedPref.getString("tokenId",""));
                    providerFormPost.setUserType(userType);

                    checkInternet(true);

//                    if(userType.equalsIgnoreCase(getString(R.string.individual_user)))
//                    {
//
//                    }
//                    else {
//
//                    }





                }

            }
        });


        return view;
    }

    private void checkInternet(boolean isPost)
    {
        NetworkConnection connection = new NetworkConnection(getActivity());
        if(connection.checkInternet()) {
            if(isPost)
                postForm();
            else
                getMasterData();
        }
        else {
            AlertBox alertBox = new AlertBox(getActivity());
            alertBox.showAlertBox(getString(R.string.no_internet_connection),View.GONE);
        }
    }



    private void postForm()
    {
        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(getActivity(), R.layout.custom_progress_bar);
        progressDialog.show();
        try {
            APIService apiService = RetroClient.getApiService();
            Call<ProviderFormModel> call = apiService.postProviderForm(providerFormPost);

            call.enqueue(new Callback<ProviderFormModel>() {
                @Override
                public void onResponse(Call<ProviderFormModel> call, Response<ProviderFormModel> response) {
                    progressDialog.dismiss();
                    try{
                        if(response.isSuccessful())
                        {
                            if("Success".equalsIgnoreCase(response.body().getResult())) {
                                AlertBox alertBox = new AlertBox(getActivity());
                                alertBox.showAlertBoxWithListener(getString(R.string.provider_form_submit), View.GONE);
                                alertBox.setOnPositiveClickListener(new AlertBox.OnPositiveClickListener() {
                                    @Override
                                    public void onPositiveClick() {
                                        getActivity().onBackPressed();
                                    }
                                });
                            }
                            else {
                                AlertBox alertBox = new AlertBox(getActivity());
                                alertBox.showAlertBox(getString(R.string.provider_form_error), View.GONE);

                            }
                        }
                        else {
                            AlertBox alertBox = new AlertBox(getActivity());
                            alertBox.showAlertBox(getString(R.string.invalid_url), View.GONE);
                        }

                    }
                    catch (Exception e)
                    {
                        AlertBox alertBox = new AlertBox(getActivity());
                        alertBox.showAlertBox(getString(R.string.invalid_response), View.GONE);
                    }
                }

                @Override
                public void onFailure(Call<ProviderFormModel> call, Throwable t) {
                    progressDialog.dismiss();
                    AlertBox alertBox = new AlertBox(getActivity());
                    alertBox.showAlertBox(getString(R.string.failed_to_reach_server), View.GONE);
                }
            });

        }
        catch (Exception e)
        {
            progressDialog.dismiss();
            AlertBox alertBox = new AlertBox(getActivity());
            alertBox.showAlertBox(getString(R.string.cannot_connect_to_server), View.GONE);
        }
    }


    private void getMasterData()
    {
        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(getActivity(), R.layout.custom_progress_bar);
        progressDialog.show();
        try{

            APIService service = RetroClient.getApiService();
            Call<MasterDropDown> call = service.getMasterData();
            call.enqueue(new Callback<MasterDropDown>() {
                @Override
                public void onResponse(Call<MasterDropDown> call, Response<MasterDropDown> response) {
                    progressDialog.dismiss();
                    try{

                        if(response.isSuccessful()) {
                            if ("Success".equalsIgnoreCase(response.body().getResult())) {
                                dropDownData = response.body().getData();
                                setUserType(dropDownData.getUserType());
                                setOccupation();
//                                occupationMsp.setItems(dropDownData.getOccupation());
//                                occupationMsp.setHint(getString(R.string.occupation));
//                                occupationSp.setSelection(dropDownData.getOccupation().size());
                                businessType.setItems(dropDownData.getTypeofBusiness());
                                businessType.setHint(getString(R.string.business_type));
                                businessType.setSelection(dropDownData.getTypeofBusiness().size());
                                toWhomContribute.setItems(dropDownData.getContribute());
                                toWhomContribute.setHint(getString(R.string.towhomiwnattocontribute));
                                toWhomContribute.setSelection(dropDownData.getContribute().size());
                                iWantProvide.setItems(dropDownData.getProvider());
                                iWantProvide.setHint(getString(R.string.iwanttoprovide));
                                iWantProvide.setSelection(dropDownData.getProvider().size());
                                iHavingHall.setItems(dropDownData.getEmergency());
                                iHavingHall.setHint(getString(R.string.haveahall));
                                iHavingHall.setSelection(dropDownData.getEmergency().size());
                                ilikeManufacture.setItems(dropDownData.getManufacture());
                                ilikeManufacture.setHint(getString(R.string.manfactureunit));
                                ilikeManufacture.setSelection(dropDownData.getManufacture().size());
                                iContributeThrough.setItems(dropDownData.getContributethrough());
                                iContributeThrough.setHint(getString(R.string.iwantcontributethrough));
                                iContributeThrough.setSelection(dropDownData.getContributethrough().size());

                            } else {
                                AlertBox alertBox = new AlertBox(getActivity());
                                alertBox.showAlertBoxWithListener(getString(R.string.nolinks), View.GONE);
                                alertBox.setOnPositiveClickListener(new AlertBox.OnPositiveClickListener() {
                                    @Override
                                    public void onPositiveClick() {
                                        getActivity().onBackPressed();
                                    }
                                });
                            }
                        }
                        else {
                            AlertBox alertBox = new AlertBox(getActivity());
                            alertBox.showAlertBoxWithListener(getString(R.string.invalid_url),View.GONE);
                            alertBox.setOnPositiveClickListener(new AlertBox.OnPositiveClickListener() {
                                @Override
                                public void onPositiveClick() {
                                    getActivity().onBackPressed();
                                }
                            });
                        }

                    }
                    catch (Exception e)
                    {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        AlertBox alertBox = new AlertBox(getActivity());
                        alertBox.showAlertBoxWithListener(getString(R.string.invalid_response),View.GONE);
                        alertBox.setOnPositiveClickListener(new AlertBox.OnPositiveClickListener() {
                            @Override
                            public void onPositiveClick() {
                                getActivity().onBackPressed();
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Call<MasterDropDown> call, Throwable t) {
                    progressDialog.dismiss();
                    AlertBox alertBox = new AlertBox(getActivity());
                    alertBox.showAlertBoxWithListener(getString(R.string.failed_to_reach_server),View.GONE);
                    alertBox.setOnPositiveClickListener(new AlertBox.OnPositiveClickListener() {
                        @Override
                        public void onPositiveClick() {
                            getActivity().onBackPressed();
                        }
                    });
                }
            });
        }catch (Exception e)
        {
            progressDialog.dismiss();
            AlertBox alertBox = new AlertBox(getActivity());
            alertBox.showAlertBoxWithListener(getString(R.string.cannot_load_data),View.GONE);
            alertBox.setOnPositiveClickListener(new AlertBox.OnPositiveClickListener() {
                @Override
                public void onPositiveClick() {
                    getActivity().onBackPressed();
                }
            });
        }

    }

    private void setOccupation()
    {
        dropDownData.getOccupation().add(0,getString(R.string.occupation));
        ThemeColorAdapter arrayAdapter = new ThemeColorAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item,dropDownData.getOccupation());
        occupationSp.setAdapter(arrayAdapter);
    }

    private void setUserType(List<String> userType)
    {
        userType.add(0,getString(R.string.choose_user_type));
        ThemeColorAdapter arrayAdapter = new ThemeColorAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item,userType);
        userTypeSp.setAdapter(arrayAdapter);
    }

    private void setTravelToSupply(List<String> list)
    {
        ThemeColorAdapter arrayAdapter = new ThemeColorAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item,list);
        travelToSupply.setAdapter(arrayAdapter);
    }

    private void setVolunteer(List<String> list)
    {
        ThemeColorAdapter arrayAdapter = new ThemeColorAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item,list);
        volunteer.setAdapter(arrayAdapter);
    }

    private void setDoShare(List<String> list)
    {
        ThemeColorAdapter arrayAdapter = new ThemeColorAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item,list);
        doShare.setAdapter(arrayAdapter);
    }

    private void setGetTouch(List<String> list)
    {
        ThemeColorAdapter arrayAdapter = new ThemeColorAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item,list);
        getTouch.setAdapter(arrayAdapter);
    }

}
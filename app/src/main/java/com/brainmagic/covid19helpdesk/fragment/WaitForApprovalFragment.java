package com.brainmagic.covid19helpdesk.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.brainmagic.covid19helpdesk.Constants;
import com.brainmagic.covid19helpdesk.R;
import com.brainmagic.covid19helpdesk.alert.AlertBox;

public class WaitForApprovalFragment extends Fragment {



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.approval_fragment, container, false);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

        SharedPreferences sharedPref = getActivity().getSharedPreferences(Constants.sharedConfig, Context.MODE_PRIVATE);
        final TextView headerTv = getActivity().findViewById(R.id.header);
        headerTv.setVisibility(View.GONE);
        CardView use_full_links=view.findViewById(R.id.usefulllinks);
        CardView latest_news=view.findViewById(R.id.latestnews);
        CardView provider=view.findViewById(R.id.provider);
        CardView helper=view.findViewById(R.id.help_cv);


        TextView nameTv = view.findViewById(R.id.name_tv);
        TextView regIdTv = view.findViewById(R.id.reg_id_tv);
        nameTv.setText(sharedPref.getString("firstName",""));
        regIdTv.setText(sharedPref.getString("tokenId",""));

        if (sharedPref.getString("userType","").equals("Needy")){
            provider.setVisibility(View.GONE);
        }else if (sharedPref.getString("userType","").equals("Provider")){
            helper.setVisibility(View.GONE);
        }

        use_full_links.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                headerTv.setVisibility(View.VISIBLE);
                FragmentManager fragmentManager=getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
                UseFullLinksFragment useFullLinksFragment=new UseFullLinksFragment();
                fragmentTransaction.replace(R.id.fragment_container,useFullLinksFragment,"useful_links");
                fragmentManager.beginTransaction();
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        provider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertBox alertBox = new AlertBox(getActivity());
                alertBox.showAlertBox(getString(R.string.wait_admin),View.GONE);
            }
        });

        helper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertBox alertBox = new AlertBox(getActivity());
                alertBox.showAlertBox(getString(R.string.wait_admin),View.GONE);
            }
        });

        latest_news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                headerTv.setVisibility(View.VISIBLE);
                FragmentManager fragmentManager=getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
                LatestNewsFragment latestNewsFragment=new LatestNewsFragment();
                fragmentTransaction.replace(R.id.fragment_container,latestNewsFragment,"latest_news");
                fragmentManager.beginTransaction();
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

//        fragmenttitle=getActivity().findViewById(R.id.fragmenttitle);
//        fragmenttitle.setText(R.string.home);

        return view;
    }
}
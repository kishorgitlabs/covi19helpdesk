package com.brainmagic.covid19helpdesk.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.brainmagic.covid19helpdesk.R;
import com.brainmagic.covid19helpdesk.adapter.ViewNeedyAdapter;
import com.brainmagic.covid19helpdesk.adapter.WareHouseAdapter;
import com.brainmagic.covid19helpdesk.model.departmentdetails.DepartmentDetailsResult;

import java.util.List;

public class WareHouseDetailsFragment extends Fragment {
    private List<DepartmentDetailsResult> data;
    private String dep;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(container.getContext()).inflate(R.layout.ware_house_fragment, container, false);
        TextView header =getActivity().findViewById(R.id.header);
        header.setText(dep);
        RecyclerView rvWareHouse = view.findViewById(R.id.ware_house_rv);
        rvWareHouse.setLayoutManager(new LinearLayoutManager(getActivity()));
        WareHouseAdapter wareHouseAdapter= new WareHouseAdapter(getActivity(),data);
        rvWareHouse.setAdapter(wareHouseAdapter);
        return view;
    }

    public void setWareHouseData(List<DepartmentDetailsResult> data, String dep)
    {
        this.data = data;
        this.dep = dep;
    }
}

package com.brainmagic.covid19helpdesk.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.FileUtils;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.brainmagic.covid19helpdesk.Constants;
import com.brainmagic.covid19helpdesk.R;
import com.brainmagic.covid19helpdesk.activities.MainActivity;
import com.brainmagic.covid19helpdesk.alert.AlertBox;
import com.brainmagic.covid19helpdesk.alert.TransparentProgressDialog;
import com.brainmagic.covid19helpdesk.model.StringModel;
import com.brainmagic.covid19helpdesk.model.api.APIService;
import com.brainmagic.covid19helpdesk.model.api.RetroClient;
import com.brainmagic.covid19helpdesk.model.fileupload.FileUploadModel;
import com.brainmagic.covid19helpdesk.model.registration.RegistrationModel;
import com.myhexaville.smartimagepicker.ImagePicker;
import com.myhexaville.smartimagepicker.OnImagePickedListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

public class PersonalDetailFragment extends Fragment {

    private ImagePicker imagePicker, filePicker;
    private ImageView selectImage;
    private int CHOOSE_FILE_REQUEST_CODE = 100;
    private static Uri contentUri = null;
    private Button uploadAddressProof;
    private SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;
    private boolean isImageUploaded = false;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.personal_detail_fragment, container, false);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

        Button uploadPhoto = view.findViewById(R.id.personal_photo_bt);
        uploadAddressProof = view.findViewById(R.id.address_proof_bt);
        selectImage = view.findViewById(R.id.personal_photo_im);
        CardView use_full_links=view.findViewById(R.id.usefulllinks);
        CardView latest_news=view.findViewById(R.id.latestnews);
        CardView provider=view.findViewById(R.id.provider);
        CardView helper=view.findViewById(R.id.help_cv);

        sharedPref = getActivity().getSharedPreferences(Constants.sharedConfig, Context.MODE_PRIVATE);
        editor = sharedPref.edit();

        uploadAddressProof.setVisibility(sharedPref.getBoolean("isImageUploaded", isImageUploaded) ? View.VISIBLE : View.GONE);

        if (sharedPref.getString("userType","").equals("Needy")){
            provider.setVisibility(View.GONE);
        }else if (sharedPref.getString("userType","").equals("Provider")){
            helper.setVisibility(View.GONE);
        }

        uploadPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imagePicker = new ImagePicker(getActivity(), null, new OnImagePickedListener() {
                    @Override
                    public void onImagePicked(Uri imageUri) {
                        imagePicker=null;
//                        imageLayout.setVisibility(View.VISIBLE);
                        selectImage.setImageURI(imageUri);
                        String mImageName = getFileName(imageUri);
                        File mImageFile = getFileFromImage(mImageName);
                        uploadPhoto(mImageFile, true, mImageName);
//                        i=imageUri;
                    }
                });
                imagePicker.choosePicture(true);
            }
        });

        use_full_links.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fragmentManager=getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
                UseFullLinksFragment useFullLinksFragment=new UseFullLinksFragment();
                fragmentTransaction.replace(R.id.fragment_container,useFullLinksFragment,"useful_links");
                fragmentManager.beginTransaction();
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        provider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertBox alertBox = new AlertBox(getActivity());
                alertBox.showAlertBox(getString(R.string.wait_admin),View.GONE);
            }
        });

        helper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertBox alertBox = new AlertBox(getActivity());
                alertBox.showAlertBox(getString(R.string.wait_admin),View.GONE);
            }
        });

        latest_news.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fragmentManager=getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
                LatestNewsFragment latestNewsFragment=new LatestNewsFragment();
                fragmentTransaction.replace(R.id.fragment_container,latestNewsFragment,"latest_news");
                fragmentManager.beginTransaction();
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        uploadAddressProof.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                openFile("application/pdf");
//                openFile("*/*");
//                openFile("image/*");
                filePicker=new ImagePicker(getActivity(), null, new OnImagePickedListener() {
                    @Override
                    public void onImagePicked(Uri imageUri) {
//                        imageLayout.setVisibility(View.VISIBLE);
//                        String selectimage.setImageURI(imageUri);
                        selectImage.setImageURI(imageUri);
                        String mImageName = getFileName(imageUri);
                        File mImageFile = getFileFromImage(mImageName);
                        uploadPhoto(mImageFile, false, mImageName);
//                        i=imageUri;
                    }
                });

                filePicker.choosePicture(true);
            }
        });

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CHOOSE_FILE_REQUEST_CODE && resultCode == RESULT_OK) {
            uploadFile(data); // currently not using so it skips this line
        } else if (resultCode == RESULT_OK) {
            if(imagePicker!=null) imagePicker.handleActivityResult(resultCode, requestCode, data);
            else filePicker.handleActivityResult(resultCode, requestCode, data);
        }
    }

    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }

        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    private File getFileFromImage(String mImageName) {
        try {
            BitmapDrawable drawable = (BitmapDrawable) selectImage.getDrawable();
            Bitmap bitmap = drawable.getBitmap();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            File directory = new File(getActivity().getFilesDir(), "profile");
            if (!directory.exists())
                directory.mkdirs();
            File myappFile = new File(directory
                    + File.separator + mImageName);
            FileOutputStream fos = new FileOutputStream(myappFile);
            fos.write(byteArray);
//                        mImageName = File_URL + myappFile.getName();
            return myappFile;
        } catch (Exception e) {
            e.printStackTrace();
            return new File("");
        }
    }


    private void uploadPhoto(File mImageFile, final boolean isPhoto, final String fileName) {
        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(getActivity(), R.layout.custom_progress_bar);
        progressDialog.show();

        try {
            APIService service = RetroClient.getApiService();
            String type = "image/png";

//            String fileType="image/png";
            RequestBody requestBody = RequestBody.create(MediaType.parse(type), mImageFile);
            MultipartBody.Part filePart = MultipartBody.Part.createFormData("image", mImageFile.getName(), requestBody);
//            RequestBody requestBody = RequestBody.create(MediaType.parse(type), mImageFile);
//            MultipartBody.Part filePart = MultipartBody.Part.createFormData("image", mImageFile.getName(), requestBody);
            final Call<FileUploadModel> request = service.fileUpload(filePart);
            request.enqueue(new Callback<FileUploadModel>() {
                @Override
                public void onResponse(Call<FileUploadModel> call, Response<FileUploadModel> response) {
//                    progressDialog.dismiss();
                    if ("Success.".equalsIgnoreCase(response.body().getMessage()))
                    {
                        if(isPhoto)
                        {
                            imageNameUpload(fileName, progressDialog);
                        }
                        else {
                            fileNameUpload(fileName, progressDialog);
                        }

                    } else {
                        progressDialog.dismiss();
                        AlertBox alert = new AlertBox(getActivity());
                        alert.showAlertBox(getString(R.string.cannot_upload_image), View.GONE);
                    }
                }

                @Override
                public void onFailure(Call<FileUploadModel> call, Throwable t) {
                    progressDialog.dismiss();
                    Log.v("Upload Exception", t.getMessage());
                    t.printStackTrace();
                    AlertBox alert = new AlertBox(getActivity());
                    alert.showAlertBox(getString(R.string.failed_to_reach_server),View.GONE);
                }

            });

        } catch (Exception ex) {
            progressDialog.dismiss();
            Log.v("Exception", ex.getMessage());
            AlertBox alert = new AlertBox(getActivity());
            alert.showAlertBox(getString(R.string.cannot_load_data), View.GONE);

        }
    }

    private void imageNameUpload(String mImageName, final TransparentProgressDialog progressDialog)
    {
//        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(getActivity(), R.layout.custom_progress_bar);
//        progressDialog.show();
        try{

            APIService service = RetroClient.getApiService();
            Call<RegistrationModel> call = service.imageNameUpload(sharedPref.getString("tokenId",""),
                    String.valueOf(sharedPref.getInt("id",0)),mImageName);
            call.enqueue(new Callback<RegistrationModel>() {
                @Override
                public void onResponse(Call<RegistrationModel> call, Response<RegistrationModel> response) {
                    progressDialog.dismiss();
                    try{

                        if("Success".equalsIgnoreCase(response.body().getResult()))
                        {

                            isImageUploaded = true;
                            editor.putBoolean("isImageUploaded", isImageUploaded);
                            editor.commit();
                            uploadAddressProof.setVisibility(View.VISIBLE);
                            AlertBox alertBox = new AlertBox(getActivity());
                            alertBox.showAlertBox(getString(R.string.photo_upload_success), View.GONE);

                        }
                        else  if("InvalidUser".equalsIgnoreCase(response.body().getResult()))
                        {
                            AlertBox alertBox = new AlertBox(getActivity());
                            alertBox.showAlertBox(getString(R.string.file_not_success), View.GONE);
                        }
                        else {
                            AlertBox alertBox = new AlertBox(getActivity());
                            alertBox.showAlertBox(getString(R.string.cannot_upload_image), View.GONE);
                        }

                    }
                    catch (Exception e)
                    {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        AlertBox alertBox = new AlertBox(getActivity());
                        alertBox.showAlertBox(getString(R.string.invalid_response),View.GONE);
//                        Toast.makeText(getActivity(), getString(R.string.invalid_response), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<RegistrationModel> call, Throwable t) {
                    progressDialog.dismiss();
                    AlertBox alertBox = new AlertBox(getActivity());
                    alertBox.showAlertBox(getString(R.string.failed_to_reach_server),View.GONE);
//                    Toast.makeText(getActivity(), getString(R.string.failed_to_reach_server), Toast.LENGTH_SHORT).show();
                }
            });
        }catch (Exception e)
        {
            progressDialog.dismiss();
            AlertBox alertBox = new AlertBox(getActivity());
            alertBox.showAlertBox(getString(R.string.cannot_load_data),View.GONE);
//            Toast.makeText(getActivity(), getString(R.string.cannot_load_data), Toast.LENGTH_SHORT).show();
        }
    }

    private void fileNameUpload(String mImageName, final TransparentProgressDialog progressDialog)
    {
//        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(getActivity(), R.layout.custom_progress_bar);
//        progressDialog.show();
        try{

            APIService service = RetroClient.getApiService();
            Call<RegistrationModel> call = service.fileNameUpload(sharedPref.getString("tokenId",""),
                    String.valueOf(sharedPref.getInt("id",0)),mImageName);
            call.enqueue(new Callback<RegistrationModel>() {
                @Override
                public void onResponse(Call<RegistrationModel> call, Response<RegistrationModel> response) {
                    progressDialog.dismiss();
                    try{

                        if("Success".equalsIgnoreCase(response.body().getResult()))
                        {
                            editor.putBoolean("isFileUploaded",true);
                            editor.commit();
                            AlertBox alertBox = new AlertBox(getActivity());
                            alertBox.showAlertBox(getString(R.string.upload_success) + getString(R.string.wait_msg),View.GONE);

                        }
                        else  if("InvalidUser".equalsIgnoreCase(response.body().getResult()))
                        {
                            AlertBox alertBox = new AlertBox(getActivity());
                            alertBox.showAlertBox(getString(R.string.file_not_success), View.GONE);
                        }
                        else {
                            AlertBox alertBox = new AlertBox(getActivity());
                            alertBox.showAlertBox(getString(R.string.cannot_upload_file), View.GONE);
                        }

                    }
                    catch (Exception e)
                    {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        AlertBox alertBox = new AlertBox(getActivity());
                        alertBox.showAlertBox(getString(R.string.invalid_response),View.GONE);
//                        Toast.makeText(getActivity(), getString(R.string.invalid_response), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<RegistrationModel> call, Throwable t) {
                    progressDialog.dismiss();
                    AlertBox alertBox = new AlertBox(getActivity());
                    alertBox.showAlertBox(getString(R.string.failed_to_reach_server),View.GONE);
//                    Toast.makeText(getActivity(), getString(R.string.failed_to_reach_server), Toast.LENGTH_SHORT).show();
                }
            });
        }catch (Exception e)
        {
            progressDialog.dismiss();
            AlertBox alertBox = new AlertBox(getActivity());
            alertBox.showAlertBox(getString(R.string.cannot_load_data),View.GONE);
//            Toast.makeText(getActivity(), getString(R.string.cannot_load_data), Toast.LENGTH_SHORT).show();
        }
    }

    private void uploadFile(Intent data) {
        File file = new File(getPath(getActivity(),data.getData()));

        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), getPath(getActivity(),data.getData()));


        APIService service = RetroClient.getApiService();
        MultipartBody.Part multipartBody = MultipartBody.Part.createFormData("file", file.getName(), requestFile);

        Call<FileUploadModel> responseBodyCall = service.fileUpload(multipartBody);
        responseBodyCall.enqueue(new Callback<FileUploadModel>() {
            @Override
            public void onResponse(Call<FileUploadModel> call, Response<FileUploadModel> response) {

                Log.d("Success", "success " + response.code());
                Log.d("Success", "success " + response.message());

            }

            @Override
            public void onFailure(Call<FileUploadModel> call, Throwable t) {
                Log.d("failure", "message = " + t.getMessage());
                Log.d("failure", "cause = " + t.getCause());
            }
        });

    }

    public String getRealPathFromURI(Uri contentUri) {
        Log.d("imin", "onClick: in image conversion");

        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = getActivity().getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DISPLAY_NAME);
            cursor.moveToFirst();
            Log.d("imin", "onClick: in image conversion try");

            return cursor.getString(column_index);
        } finally {
            Log.d("imin", "onClick: in image conversion finally");

            if (cursor != null) {
                cursor.close();
            }
        }
    }

    @SuppressLint("NewApi")
    public String getPath(final Context context, final Uri uri) {
        // check here to KITKAT or new version
        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
        String selection = null;
        String[] selectionArgs = null;
        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                String fullPath = getPathFromExtSD(split);
                if (fullPath != "") {
                    return fullPath;
                } else {
                    return null;
                }
            }

            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    final String id;
                    Cursor cursor = null;
                    try {
                        cursor = context.getContentResolver().query(uri, new String[]{MediaStore.MediaColumns.DISPLAY_NAME}, null, null, null);
                        if (cursor != null && cursor.moveToFirst()) {
                            String fileName = cursor.getString(0);
                            String path = Environment.getExternalStorageDirectory().toString() + "/Download/" + fileName;
                            if (!TextUtils.isEmpty(path)) {
                                return path;
                            }
                        }
                    } finally {
                        if (cursor != null)
                            cursor.close();
                    }
                    id = DocumentsContract.getDocumentId(uri);
                    if (!TextUtils.isEmpty(id)) {
                        if (id.startsWith("raw:")) {
                            return id.replaceFirst("raw:", "");
                        }
                        String[] contentUriPrefixesToTry = new String[]{
                                "content://downloads/public_downloads",
                                "content://downloads/my_downloads"
                        };
                        for (String contentUriPrefix : contentUriPrefixesToTry) {
                            try {
                                final Uri contentUri = ContentUris.withAppendedId(Uri.parse(contentUriPrefix), Long.valueOf(id));

                         /*   final Uri contentUri = ContentUris.withAppendedId(
                                    Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));*/

                                return getDataColumn(context, contentUri, null, null);
                            } catch (NumberFormatException e) {
                                //In Android 8 and Android P the id is not a number
                                return uri.getPath().replaceFirst("^/document/raw:", "").replaceFirst("^raw:", "");
                            }
                        }


                    }

                } else {
                    final String id = DocumentsContract.getDocumentId(uri);
                    final boolean isOreo = Build.VERSION.SDK_INT >= Build.VERSION_CODES.O;
                    if (id.startsWith("raw:")) {
                        return id.replaceFirst("raw:", "");
                    }
                    try {
                        contentUri = ContentUris.withAppendedId(
                                Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                    if (contentUri != null) {
                        return getDataColumn(context, contentUri, null, null);
                    }
                }
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {

                    String mImageName = getFileName(uri);
                 return getFileFromImage(mImageName).getPath();
//                final String docId = DocumentsContract.getDocumentId(uri);
//                final String[] split = docId.split(":");
//                final String type = split[0];
//
//                Uri contentUri = null;
//
//                if ("image".equals(type)) {
//                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
//                } else if ("video".equals(type)) {
//                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
//                } else if ("audio".equals(type)) {
//                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
//                }
//                selection = "_id=?";
//                selectionArgs = new String[]{split[1]};
//
//
//                return getDataColumn(context, contentUri, selection,
//                        selectionArgs);
            }

        }
            return null;

    }

    private static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    private String getDataColumn(Context context, Uri uri,
                                        String selection, String[] selectionArgs) {
        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {column};

        try {
            cursor = context.getContentResolver().query(uri, projection,
                    selection, selectionArgs, null);

            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }

        return null;
    }

    private boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }


    private String getPathFromExtSD(String[] pathData) {
        final String type = pathData[0];
        final String relativePath = "/" + pathData[1];
        String fullPath = "";

        // on my Sony devices (4.4.4 & 5.1.1), `type` is a dynamic string
        // something like "71F8-2C0A", some kind of unique id per storage
        // don't know any API that can get the root path of that storage based on its id.
        //
        // so no "primary" type, but let the check here for other devices
        if ("primary".equalsIgnoreCase(type)) {
            fullPath = Environment.getExternalStorageDirectory() + relativePath;
            if (fileExists(fullPath)) {
                return fullPath;
            }
        }

        // Environment.isExternalStorageRemovable() is `true` for external and internal storage
        // so we cannot relay on it.
        //
        // instead, for each possible path, check if file exists
        // we'll start with secondary storage as this could be our (physically) removable sd card
        fullPath = System.getenv("SECONDARY_STORAGE") + relativePath;
        if (fileExists(fullPath)) {
            return fullPath;
        }

        fullPath = System.getenv("EXTERNAL_STORAGE") + relativePath;
        if (fileExists(fullPath)) {
            return fullPath;
        }

        return fullPath;
    }

    private boolean fileExists(String filePath) {
        File file = new File(filePath);

        return file.exists();
    }

    private boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }




    public void openFile(String mimeType) {

//        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        Intent intent = new Intent(Intent.ACTION_CHOOSER);
//        intent.setType(mimeType);
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        // special intent for Samsung file manager
        Intent sIntent = new Intent("com.sec.android.app.myfiles.PICK_DATA");
        // if you want any file type, you can skip next line
        sIntent.putExtra("CONTENT_TYPE", mimeType);
        sIntent.putExtra(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        sIntent.addCategory(Intent.CATEGORY_DEFAULT);

        Intent chooserIntent;
        if (getActivity().getPackageManager().resolveActivity(sIntent, 0) != null){
            // it is device with Samsung file manager
            chooserIntent = Intent.createChooser(sIntent, "Open file");
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] { intent});
        } else {
            chooserIntent = Intent.createChooser(intent, "Open file");
        }

        try {
            startActivityForResult(chooserIntent, CHOOSE_FILE_REQUEST_CODE);
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getActivity(), "No suitable File Manager was found.", Toast.LENGTH_SHORT).show();
        }
    }

}

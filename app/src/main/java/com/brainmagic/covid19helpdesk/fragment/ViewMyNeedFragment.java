package com.brainmagic.covid19helpdesk.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.brainmagic.covid19helpdesk.Constants;
import com.brainmagic.covid19helpdesk.R;
import com.brainmagic.covid19helpdesk.adapter.ViewNeedyAdapter;
import com.brainmagic.covid19helpdesk.alert.AlertBox;
import com.brainmagic.covid19helpdesk.alert.TransparentProgressDialog;
import com.brainmagic.covid19helpdesk.model.api.APIService;
import com.brainmagic.covid19helpdesk.model.api.RetroClient;
import com.brainmagic.covid19helpdesk.model.viewneeds.ViewNeedsModel;
import com.brainmagic.covid19helpdesk.network.NetworkConnection;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewMyNeedFragment extends Fragment {

    private SharedPreferences sharedPref;
    private RecyclerView viewNeedyRv;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.view_provider_details, container, false);
        sharedPref = getActivity().getSharedPreferences(Constants.sharedConfig, Context.MODE_PRIVATE);

        TextView headerTv =getActivity().findViewById(R.id.header);
        headerTv.setText(R.string.view_needy);

        viewNeedyRv =view.findViewById(R.id.view_provider_rv);
        checkInternet();
        return view;
    }

    private void checkInternet()
    {
        NetworkConnection connection = new NetworkConnection(getActivity());
        if(connection.checkInternet()) {
            viewOtherNeeds();
        }
        else {
            AlertBox alertBox = new AlertBox(getActivity());
            alertBox.showAlertBox(getString(R.string.no_internet_connection),View.GONE);
        }
    }

    private void viewOtherNeeds()
    {
        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(getActivity(), R.layout.custom_progress_bar);
        progressDialog.show();
        try{

            APIService service = RetroClient.getApiService();
            Call<ViewNeedsModel> call = service.viewNeedy(sharedPref.getString("tokenId",""));
            call.enqueue(new Callback<ViewNeedsModel>() {
                @Override
                public void onResponse(Call<ViewNeedsModel> call, Response<ViewNeedsModel> response) {
                    progressDialog.dismiss();
                    try{
                        if(response.isSuccessful()) {
                            if ("Success".equalsIgnoreCase(response.body().getResult())) {
                                viewNeedyRv.setLayoutManager(new LinearLayoutManager(getActivity()));
                                ViewNeedyAdapter viewProviderAdapter = new ViewNeedyAdapter(getActivity(), response.body().getData(), "myNeed");
                                viewNeedyRv.setAdapter(viewProviderAdapter);
                            } else {
                                AlertBox alertBox = new AlertBox(getActivity());
                                alertBox.showAlertBoxWithListener(getString(R.string.no_record), View.GONE);
                                alertBox.setOnPositiveClickListener(new AlertBox.OnPositiveClickListener() {
                                    @Override
                                    public void onPositiveClick() {
                                        getActivity().onBackPressed();
                                    }
                                });
                            }
                        }else {
                            AlertBox alertBox = new AlertBox(getActivity());
                            alertBox.showAlertBoxWithListener(getString(R.string.invalid_url), View.GONE);
                            alertBox.setOnPositiveClickListener(new AlertBox.OnPositiveClickListener() {
                                @Override
                                public void onPositiveClick() {
                                    getActivity().onBackPressed();
                                }
                            });
                        }

                    }
                    catch (Exception e)
                    {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        AlertBox alertBox = new AlertBox(getActivity());
                        alertBox.showAlertBoxWithListener(getString(R.string.invalid_response),View.GONE);
                        alertBox.setOnPositiveClickListener(new AlertBox.OnPositiveClickListener() {
                            @Override
                            public void onPositiveClick() {
                                getActivity().onBackPressed();
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Call<ViewNeedsModel> call, Throwable t) {
                    progressDialog.dismiss();
                    AlertBox alertBox = new AlertBox(getActivity());
                    alertBox.showAlertBoxWithListener(getString(R.string.failed_to_reach_server),View.GONE);
                    alertBox.setOnPositiveClickListener(new AlertBox.OnPositiveClickListener() {
                        @Override
                        public void onPositiveClick() {
                            getActivity().onBackPressed();
                        }
                    });
                }
            });
        }catch (Exception e)
        {
            progressDialog.dismiss();
            AlertBox alertBox = new AlertBox(getActivity());
            alertBox.showAlertBoxWithListener(getString(R.string.cannot_load_data),View.GONE);
            alertBox.setOnPositiveClickListener(new AlertBox.OnPositiveClickListener() {
                @Override
                public void onPositiveClick() {
                    getActivity().onBackPressed();
                }
            });
        }

    }
}

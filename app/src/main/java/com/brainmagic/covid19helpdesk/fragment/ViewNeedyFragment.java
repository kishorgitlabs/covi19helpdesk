package com.brainmagic.covid19helpdesk.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.brainmagic.covid19helpdesk.Constants;
import com.brainmagic.covid19helpdesk.R;
import com.brainmagic.covid19helpdesk.adapter.ViewNeedyAdapter;
import com.brainmagic.covid19helpdesk.alert.AlertBox;
import com.brainmagic.covid19helpdesk.alert.TransparentProgressDialog;
import com.brainmagic.covid19helpdesk.model.StringListModel;
import com.brainmagic.covid19helpdesk.model.StringModel;
import com.brainmagic.covid19helpdesk.model.api.APIService;
import com.brainmagic.covid19helpdesk.model.api.RetroClient;
import com.brainmagic.covid19helpdesk.model.viewneeds.ViewNeedsModel;
import com.brainmagic.covid19helpdesk.model.viewneeds.ViewNeedsResult;
import com.brainmagic.covid19helpdesk.network.NetworkConnection;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewNeedyFragment extends Fragment {

    private RecyclerView viewNeedyRv;
    private Spinner requestListSp;
    private List<ViewNeedsResult> data;
    private ViewNeedyAdapter viewProviderAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.view_provider_details, container, false);
        TextView headerTv =getActivity().findViewById(R.id.header);
        requestListSp = view.findViewById(R.id.request_list_sp);
        View lineView = view.findViewById(R.id.line_view);
        headerTv.setText(R.string.view_needy);

        viewNeedyRv =view.findViewById(R.id.view_provider_rv);
        requestListSp.setVisibility(View.VISIBLE);
        lineView.setVisibility(View.VISIBLE);

        requestListSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String value = parent.getAdapter().getItem(position).toString();
                if(value.equalsIgnoreCase("Search by Request"))
                {
                    //load full data
                    if(viewProviderAdapter != null)
                    {
                        viewProviderAdapter.setData(data);
                        viewProviderAdapter.notifyDataSetChanged();
                    }
                }
                else {
                    loadDataBasedSpinner(value);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



        checkInternet();

        return view;
    }

    private void loadDataBasedSpinner(String value)
    {
        List<ViewNeedsResult> tempData = new ArrayList<>();
        for (ViewNeedsResult needs : data)
        {
            if(needs.getIneed() != null)
                if(needs.getIneed().contains(value))
                {
                    tempData.add(needs);
                }
        }
        if(tempData.size() != 0)
        {
            viewProviderAdapter.setData(tempData);
            viewProviderAdapter.notifyDataSetChanged();
        }
        else {
            AlertBox alertBox = new AlertBox(getActivity());
            alertBox.showAlertBoxWithListener(getString(R.string.no_record),View.GONE);
            alertBox.setOnPositiveClickListener(new AlertBox.OnPositiveClickListener() {
                @Override
                public void onPositiveClick() {
                    requestListSp.setSelection(0);
                }
            });
        }
    }

    private void loadSpinner()
    {
//        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(getActivity(), R.layout.custom_progress_bar);
//        progressDialog.show();
        try{

            APIService service = RetroClient.getApiService();
            Call<StringListModel> call = service.needsList();
            call.enqueue(new Callback<StringListModel>() {
                @Override
                public void onResponse(Call<StringListModel> call, Response<StringListModel> response) {
//                    progressDialog.dismiss();
                    try{
                        if(response.isSuccessful()) {
                            if ("Success".equalsIgnoreCase(response.body().getResult())) {
                                List<String> needList = response.body().getData();
                                needList.add(0,"Search by Request");
                                ArrayAdapter arrayAdapter = new ArrayAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item,needList);
                                requestListSp.setAdapter(arrayAdapter);
                            }

                        }

                    }
                    catch (Exception e)
                    {
//                        progressDialog.dismiss();
                        e.printStackTrace();

                    }
                }

                @Override
                public void onFailure(Call<StringListModel> call, Throwable t) {
//                    progressDialog.dismiss();

                }
            });
        }catch (Exception e)
        {
//            progressDialog.dismiss();

        }
    }

    private void checkInternet()
    {
        NetworkConnection connection = new NetworkConnection(getActivity());
        if(connection.checkInternet()) {
            loadSpinner();
            viewOtherNeeds();
        }
        else {
            AlertBox alertBox = new AlertBox(getActivity());
            alertBox.showAlertBox(getString(R.string.no_internet_connection),View.GONE);
        }
    }

    private void viewOtherNeeds()
    {
        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(getActivity(), R.layout.custom_progress_bar);
        progressDialog.show();
        try{

            APIService service = RetroClient.getApiService();
            Call<ViewNeedsModel> call = service.viewOtherNeeds();
            call.enqueue(new Callback<ViewNeedsModel>() {
                @Override
                public void onResponse(Call<ViewNeedsModel> call, Response<ViewNeedsModel> response) {
                    progressDialog.dismiss();
                    try{
                        if(response.isSuccessful()) {
                            if ("Success".equalsIgnoreCase(response.body().getResult())) {
                                data = response.body().getData();
                                viewNeedyRv.setLayoutManager(new LinearLayoutManager(getActivity()));
                                viewProviderAdapter = new ViewNeedyAdapter(getActivity(), response.body().getData(),"needy");
                                viewNeedyRv.setAdapter(viewProviderAdapter);
                            } else {
                                AlertBox alertBox = new AlertBox(getActivity());
                                alertBox.showAlertBoxWithListener(getString(R.string.no_record), View.GONE);
                                alertBox.setOnPositiveClickListener(new AlertBox.OnPositiveClickListener() {
                                    @Override
                                    public void onPositiveClick() {
                                        getActivity().onBackPressed();
                                    }
                                });
                            }
                        }else {
                            AlertBox alertBox = new AlertBox(getActivity());
                            alertBox.showAlertBoxWithListener(getString(R.string.invalid_url), View.GONE);
                            alertBox.setOnPositiveClickListener(new AlertBox.OnPositiveClickListener() {
                                @Override
                                public void onPositiveClick() {
                                    getActivity().onBackPressed();
                                }
                            });
                        }

                    }
                    catch (Exception e)
                    {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        AlertBox alertBox = new AlertBox(getActivity());
                        alertBox.showAlertBoxWithListener(getString(R.string.invalid_response),View.GONE);
                        alertBox.setOnPositiveClickListener(new AlertBox.OnPositiveClickListener() {
                            @Override
                            public void onPositiveClick() {
                                getActivity().onBackPressed();
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Call<ViewNeedsModel> call, Throwable t) {
                    progressDialog.dismiss();
                    AlertBox alertBox = new AlertBox(getActivity());
                    alertBox.showAlertBoxWithListener(getString(R.string.failed_to_reach_server),View.GONE);
                    alertBox.setOnPositiveClickListener(new AlertBox.OnPositiveClickListener() {
                        @Override
                        public void onPositiveClick() {
                            getActivity().onBackPressed();
                        }
                    });
                }
            });
        }catch (Exception e)
        {
            progressDialog.dismiss();
            AlertBox alertBox = new AlertBox(getActivity());
            alertBox.showAlertBoxWithListener(getString(R.string.cannot_load_data),View.GONE);
            alertBox.setOnPositiveClickListener(new AlertBox.OnPositiveClickListener() {
                @Override
                public void onPositiveClick() {
                    getActivity().onBackPressed();
                }
            });
        }

    }
}

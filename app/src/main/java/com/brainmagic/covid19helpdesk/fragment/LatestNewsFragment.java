package com.brainmagic.covid19helpdesk.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.brainmagic.covid19helpdesk.R;
import com.brainmagic.covid19helpdesk.adapter.LatestNewsAdapter;
import com.brainmagic.covid19helpdesk.alert.AlertBox;
import com.brainmagic.covid19helpdesk.alert.TransparentProgressDialog;
import com.brainmagic.covid19helpdesk.model.api.APIService;
import com.brainmagic.covid19helpdesk.model.api.RetroClient;
import com.brainmagic.covid19helpdesk.model.latestnews.LatestNews;
import com.brainmagic.covid19helpdesk.model.latestnews.LatestNewsData;
import com.brainmagic.covid19helpdesk.network.NetworkConnection;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LatestNewsFragment extends Fragment {

    private TextView fragmenttitle;
    private List<LatestNewsData> latestNewsData;
    private RecyclerView latestnewslist;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.latest_news, container, false);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

        latestnewslist=view.findViewById(R.id.latestnewslist);
        fragmenttitle=getActivity().findViewById(R.id.header);
        latestnewslist.setLayoutManager(new LinearLayoutManager(getActivity()));
        fragmenttitle.setText(R.string.latestnews);
        checkInternet();
        return view;
    }

    private void checkInternet()
    {
        NetworkConnection connection = new NetworkConnection(getActivity());
        if(connection.checkInternet()) {
            getLatestNews();
        }
        else {
            AlertBox alertBox = new AlertBox(getActivity());
            alertBox.showAlertBox(getString(R.string.no_internet_connection),View.GONE);

        }
    }


    private void getLatestNews()
    {
        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(getActivity(), R.layout.custom_progress_bar);
        progressDialog.show();
        try{

            APIService service = RetroClient.getApiService();
            Call<LatestNews> call = service.getLatestNews();
            call.enqueue(new Callback<LatestNews>() {
                @Override
                public void onResponse(Call<LatestNews> call, Response<LatestNews> response) {
                    progressDialog.dismiss();
                    try{

                        if("Success".equalsIgnoreCase(response.body().getResult()))
                        {
                            latestNewsData=response.body().getData();
                            LatestNewsAdapter latestNewsAdapter =new LatestNewsAdapter(getActivity(),latestNewsData);
                            latestnewslist.setAdapter(latestNewsAdapter);
                        }
                        else {
                            AlertBox alertBox = new AlertBox(getActivity());
                            alertBox.showAlertBoxWithListener(getString(R.string.no_record),View.GONE);
                            alertBox.setOnPositiveClickListener(new AlertBox.OnPositiveClickListener() {
                                @Override
                                public void onPositiveClick() {
                                    getActivity().onBackPressed();
                                }
                            });
                        }

                    }
                    catch (Exception e)
                    {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        AlertBox alertBox = new AlertBox(getActivity());
                        alertBox.showAlertBoxWithListener(getString(R.string.invalid_response),View.GONE);
                        alertBox.setOnPositiveClickListener(new AlertBox.OnPositiveClickListener() {
                            @Override
                            public void onPositiveClick() {
                                getActivity().onBackPressed();
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Call<LatestNews> call, Throwable t) {
                    progressDialog.dismiss();
                    AlertBox alertBox = new AlertBox(getActivity());
                    alertBox.showAlertBoxWithListener(getString(R.string.failed_to_reach_server),View.GONE);
                    alertBox.setOnPositiveClickListener(new AlertBox.OnPositiveClickListener() {
                        @Override
                        public void onPositiveClick() {
                            getActivity().onBackPressed();
                        }
                    });
                }
            });
        }catch (Exception e)
        {
            progressDialog.dismiss();
            AlertBox alertBox = new AlertBox(getActivity());
            alertBox.showAlertBoxWithListener(getString(R.string.cannot_load_data),View.GONE);
            alertBox.setOnPositiveClickListener(new AlertBox.OnPositiveClickListener() {
                @Override
                public void onPositiveClick() {
                    getActivity().onBackPressed();
                }
            });
        }

    }
}
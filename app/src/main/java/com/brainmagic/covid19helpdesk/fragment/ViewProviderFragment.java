package com.brainmagic.covid19helpdesk.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.brainmagic.covid19helpdesk.Constants;
import com.brainmagic.covid19helpdesk.R;
import com.brainmagic.covid19helpdesk.adapter.UseFullLInksAdapter;
import com.brainmagic.covid19helpdesk.adapter.ViewProviderAdapter;
import com.brainmagic.covid19helpdesk.alert.AlertBox;
import com.brainmagic.covid19helpdesk.alert.TransparentProgressDialog;
import com.brainmagic.covid19helpdesk.model.api.APIService;
import com.brainmagic.covid19helpdesk.model.api.RetroClient;
import com.brainmagic.covid19helpdesk.model.usefulllinks.UseFullData;
import com.brainmagic.covid19helpdesk.model.usefulllinks.UseFullLinksResult;
import com.brainmagic.covid19helpdesk.model.viewproviders.ViewProviders;
import com.brainmagic.covid19helpdesk.model.viewproviders.ViewProvidersData;
import com.brainmagic.covid19helpdesk.network.NetworkConnection;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewProviderFragment extends Fragment {

    private TextView fragmenttitle;
    private RecyclerView view_provider_rv;
    private List<ViewProvidersData> viewProvidersData;
    private SharedPreferences sharedPref;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.view_provider_details, container, false);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
         sharedPref = getActivity().getSharedPreferences(Constants.sharedConfig, Context.MODE_PRIVATE);
        view_provider_rv=view.findViewById(R.id.view_provider_rv);
        fragmenttitle=getActivity().findViewById(R.id.header);
        fragmenttitle.setText(R.string.view_providers);

        view_provider_rv.setLayoutManager(new LinearLayoutManager(getActivity()));

        checkInternet();
        return view;
    }

    private void checkInternet()
    {
        NetworkConnection connection = new NetworkConnection(getActivity());
        if(connection.checkInternet()) {
            if (getArguments().getString("provideType").equals("All")){
                getProviderDetails();
            }
            else {
                getProviderDetailsForUser();
            }

        }
        else {
            AlertBox alertBox = new AlertBox(getActivity());
            alertBox.showAlertBox(getString(R.string.no_internet_connection),View.GONE);

        }
    }



    private void getProviderDetails()
    {
        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(getActivity(), R.layout.custom_progress_bar);
        progressDialog.show();
        try{
            APIService service = RetroClient.getApiService();
            Call<ViewProviders> call = service.getProviders();
            call.enqueue(new Callback<ViewProviders>() {
                @Override
                public void onResponse(Call<ViewProviders> call, Response<ViewProviders> response) {
                    progressDialog.dismiss();
                    try{
                        if("Success".equalsIgnoreCase(response.body().getResult()))
                        {
                            viewProvidersData=response.body().getData();
                            ViewProviderAdapter viewProviderAdapter=new ViewProviderAdapter(getActivity(),viewProvidersData,"All");
                            view_provider_rv.setAdapter(viewProviderAdapter);
                        }
                        else {
                            AlertBox alertBox = new AlertBox(getActivity());
                            alertBox.showAlertBoxWithListener(getString(R.string.no_record),View.GONE);
                            alertBox.setOnPositiveClickListener(new AlertBox.OnPositiveClickListener() {
                                @Override
                                public void onPositiveClick() {
                                    getActivity().onBackPressed();
                                }
                            });
                        }
                    }
                    catch (Exception e)
                    {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        AlertBox alertBox = new AlertBox(getActivity());
                        alertBox.showAlertBoxWithListener(getString(R.string.invalid_response),View.GONE);
                        alertBox.setOnPositiveClickListener(new AlertBox.OnPositiveClickListener() {
                            @Override
                            public void onPositiveClick() {
                                getActivity().onBackPressed();
                            }
                        });
                    }
                }
                @Override
                public void onFailure(Call<ViewProviders> call, Throwable t) {
                    progressDialog.dismiss();
                    AlertBox alertBox = new AlertBox(getActivity());
                    alertBox.showAlertBoxWithListener(getString(R.string.failed_to_reach_server),View.GONE);
                    alertBox.setOnPositiveClickListener(new AlertBox.OnPositiveClickListener() {
                        @Override
                        public void onPositiveClick() {
                            getActivity().onBackPressed();
                        }
                    });
                }
            });
        }catch (Exception e)
        {
            progressDialog.dismiss();
            AlertBox alertBox = new AlertBox(getActivity());
            alertBox.showAlertBoxWithListener(getString(R.string.cannot_load_data),View.GONE);
            alertBox.setOnPositiveClickListener(new AlertBox.OnPositiveClickListener() {
                @Override
                public void onPositiveClick() {
                    getActivity().onBackPressed();
                }
            });
        }

    }



    private void getProviderDetailsForUser()
    {
        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(getActivity(), R.layout.custom_progress_bar);
        progressDialog.show();
        try{
            APIService service = RetroClient.getApiService();
            Call<ViewProviders> call = service.getProvidersDetails(sharedPref.getString("tokenId",""));
            call.enqueue(new Callback<ViewProviders>() {
                @Override
                public void onResponse(Call<ViewProviders> call, Response<ViewProviders> response) {
                    progressDialog.dismiss();
                    try{
                        if("Success".equalsIgnoreCase(response.body().getResult()))
                        {
                            viewProvidersData=response.body().getData();
                            ViewProviderAdapter viewProviderAdapter=new ViewProviderAdapter(getActivity(),viewProvidersData,"Provider");
                            view_provider_rv.setAdapter(viewProviderAdapter);
                        }
                        else {
                            AlertBox alertBox = new AlertBox(getActivity());
                            alertBox.showAlertBoxWithListener(getString(R.string.no_record),View.GONE);
                            alertBox.setOnPositiveClickListener(new AlertBox.OnPositiveClickListener() {
                                @Override
                                public void onPositiveClick() {
                                    getActivity().onBackPressed();
                                }
                            });
                        }
                    }
                    catch (Exception e)
                    {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        AlertBox alertBox = new AlertBox(getActivity());
                        alertBox.showAlertBoxWithListener(getString(R.string.invalid_response),View.GONE);
                        alertBox.setOnPositiveClickListener(new AlertBox.OnPositiveClickListener() {
                            @Override
                            public void onPositiveClick() {
                                getActivity().onBackPressed();
                            }
                        });
                    }
                }
                @Override
                public void onFailure(Call<ViewProviders> call, Throwable t) {
                    progressDialog.dismiss();
                    AlertBox alertBox = new AlertBox(getActivity());
                    alertBox.showAlertBoxWithListener(getString(R.string.failed_to_reach_server),View.GONE);
                    alertBox.setOnPositiveClickListener(new AlertBox.OnPositiveClickListener() {
                        @Override
                        public void onPositiveClick() {
                            getActivity().onBackPressed();
                        }
                    });
                }
            });
        }catch (Exception e)
        {
            progressDialog.dismiss();
            AlertBox alertBox = new AlertBox(getActivity());
            alertBox.showAlertBoxWithListener(getString(R.string.cannot_load_data),View.GONE);
            alertBox.setOnPositiveClickListener(new AlertBox.OnPositiveClickListener() {
                @Override
                public void onPositiveClick() {
                    getActivity().onBackPressed();
                }
            });
        }

    }
}
package com.brainmagic.covid19helpdesk.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.brainmagic.covid19helpdesk.Constants;
import com.brainmagic.covid19helpdesk.R;
import com.brainmagic.covid19helpdesk.alert.AlertBox;
import com.brainmagic.covid19helpdesk.alert.TransparentProgressDialog;
import com.brainmagic.covid19helpdesk.model.StringListModel;
import com.brainmagic.covid19helpdesk.model.api.APIService;
import com.brainmagic.covid19helpdesk.model.api.RetroClient;
import com.brainmagic.covid19helpdesk.model.departmentdetails.DepartmentDetailsModel;
import com.brainmagic.covid19helpdesk.model.versioncheck.VersionCheck;
import com.brainmagic.covid19helpdesk.network.NetworkConnection;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment {

    private TextView fragmenttitle;
    private CardView usefulllinks,latestnews,provider, helper,view_providers_cv,my_provide_cv;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.home_fragment, container, false);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        SharedPreferences sharedPref = getActivity().getSharedPreferences(Constants.sharedConfig, Context.MODE_PRIVATE);

        usefulllinks=view.findViewById(R.id.usefulllinks);
        helper=view.findViewById(R.id.help_cv);
        latestnews=view.findViewById(R.id.latestnews);
        view_providers_cv=view.findViewById(R.id.view_providers_cv);
        my_provide_cv=view.findViewById(R.id.my_provide_cv);
        provider=view.findViewById(R.id.provider);
        CardView viewNeedy=view.findViewById(R.id.view_needy);
        CardView viewMyNeed=view.findViewById(R.id.view_my_need);
        CardView cvSearch=view.findViewById(R.id.search);
        fragmenttitle=getActivity().findViewById(R.id.header);
        fragmenttitle.setText(R.string.home);

        TextView nameTv = view.findViewById(R.id.name_tv);
        TextView regIdTv = view.findViewById(R.id.reg_id_tv);
        nameTv.setText(sharedPref.getString("firstName",""));
        regIdTv.setText(sharedPref.getString("tokenId",""));

        if (sharedPref.getString("userType","").equals("Needy")){
            provider.setVisibility(View.GONE);
        }else if (sharedPref.getString("userType","").equals("Provider")){
            helper.setVisibility(View.GONE);
        }

        cvSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkInternetForStateList();

            }
        });

        viewMyNeed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager=getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
                ViewMyNeedFragment viewMyNeedFragment=new ViewMyNeedFragment();
                fragmentTransaction.replace(R.id.fragment_container,viewMyNeedFragment,"viewMyNeed");
                fragmentManager.beginTransaction();
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        viewNeedy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager=getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
                ViewNeedyFragment viewNeedyFragment=new ViewNeedyFragment();
                fragmentTransaction.replace(R.id.fragment_container,viewNeedyFragment,"viewNeedy");
                fragmentManager.beginTransaction();
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });


        usefulllinks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fragmentManager=getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
                UseFullLinksFragment useFullLinksFragment=new UseFullLinksFragment();
                fragmentTransaction.replace(R.id.fragment_container,useFullLinksFragment,"useful_links");
                fragmentManager.beginTransaction();
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        latestnews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fragmentManager=getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
                LatestNewsFragment latestNewsFragment=new LatestNewsFragment();
                fragmentTransaction.replace(R.id.fragment_container,latestNewsFragment,"latest_news");
                fragmentManager.beginTransaction();
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        provider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fragmentManager=getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
                ProviderFragment providerFragment=new ProviderFragment();
                fragmentTransaction.replace(R.id.fragment_container,providerFragment,"provider");
                fragmentManager.beginTransaction();
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        helper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fragmentManager=getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
                HelperFragment helperFragment=new HelperFragment();
                fragmentTransaction.replace(R.id.fragment_container,helperFragment,"helper");
                fragmentManager.beginTransaction();
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });
        view_providers_cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle provideType=new Bundle();
                provideType.putString("provideType","All");
                FragmentManager fragmentManager=getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
                ViewProviderFragment viewProviderFragment=new ViewProviderFragment();
                fragmentTransaction.replace(R.id.fragment_container,viewProviderFragment,"viewprovider");
                viewProviderFragment.setArguments(provideType);
                fragmentManager.beginTransaction();
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        my_provide_cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle provideType=new Bundle();
                provideType.putString("provideType","Provider");
                FragmentManager fragmentManager=getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
                ViewProviderFragment viewProviderFragment=new ViewProviderFragment();
                fragmentTransaction.replace(R.id.fragment_container,viewProviderFragment,"viewprovider");
                viewProviderFragment.setArguments(provideType);
                fragmentManager.beginTransaction();
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });



        checkInternet();

        return view;
    }

    private void checkInternet()
    {
        NetworkConnection connection = new NetworkConnection(getActivity());
        if(connection.checkInternet())
        {
            versionCheck();
        }
    }

    private void checkInternetForStateList()
    {
        NetworkConnection connection = new NetworkConnection(getActivity());
        if(connection.checkInternet())
        {
            getStateList();
        }
        else {

            AlertBox alertBox = new AlertBox(getActivity());
            alertBox.showAlertBox(getString(R.string.no_internet_connection),View.GONE);
        }
    }

    private void versionCheck()
    {
//        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(getActivity(), R.layout.custom_progress_bar);
//        progressDialog.show();
        try{
            PackageManager manager = getActivity().getPackageManager();
            PackageInfo info = null;
            info = manager.getPackageInfo(
                    getActivity().getPackageName(), 0);
            int version = info.versionCode;
//            Log.d(TAG, "onCreateView: "+version);

            APIService service = RetroClient.getApiService();
            Call<VersionCheck> call = service.versionCheck(version);
            call.enqueue(new Callback<VersionCheck>() {
                @Override
                public void onResponse(Call<VersionCheck> call, Response<VersionCheck> response) {
//                    progressDialog.dismiss();
                    try{

                        if(response.isSuccessful()) {
                            if ("Success".equalsIgnoreCase(response.body().getResult())) {
                                AlertBox alertBox = new AlertBox(getActivity());
                                alertBox.showAlertBoxWithListener(getString(R.string.updateavailable), View.GONE);
                                alertBox.setOnPositiveClickListener(new AlertBox.OnPositiveClickListener() {
                                    @Override
                                    public void onPositiveClick() {
                                        Intent openplaystore = new Intent(android.content.Intent.ACTION_VIEW);
                                        openplaystore.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.brainmagic.covid19helpdesk"));
                                        startActivity(openplaystore);
                                    }
                                });
                            }
                        }
                    }
                    catch (Exception e)
                    {
//                        progressDialog.dismiss();
//                        e.printStackTrace();
//                        AlertBox alertBox = new AlertBox(getActivity());
//                        alertBox.showAlertBoxWithListener(getString(R.string.invalid_response),View.GONE);
//                        alertBox.setOnPositiveClickListener(new AlertBox.OnPositiveClickListener() {
//                            @Override
//                            public void onPositiveClick() {
//                                getActivity().onBackPressed();
//                            }
//                        });
                    }
                }

                @Override
                public void onFailure(Call<VersionCheck> call, Throwable t) {
//                    progressDialog.dismiss();
//                    AlertBox alertBox = new AlertBox(getActivity());
//                    alertBox.showAlertBoxWithListener(getString(R.string.failed_to_reach_server),View.GONE);
//                    alertBox.setOnPositiveClickListener(new AlertBox.OnPositiveClickListener() {
//                        @Override
//                        public void onPositiveClick() {
//                            getActivity().onBackPressed();
//                        }
//                    });
                }
            });
        }catch (Exception e)
        {
//            progressDialog.dismiss();
//            AlertBox alertBox = new AlertBox(getActivity());
//            alertBox.showAlertBoxWithListener(getString(R.string.cannot_load_data),View.GONE);
//            alertBox.setOnPositiveClickListener(new AlertBox.OnPositiveClickListener() {
//                @Override
//                public void onPositiveClick() {
//                    getActivity().onBackPressed();
//                }
//            });
        }

    }

    private void getStateList()
    {
        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(getActivity(), R.layout.custom_progress_bar);
        progressDialog.show();
            try{

                APIService service = RetroClient.getApiService();
                Call<StringListModel> call = service.stateList();
                call.enqueue(new Callback<StringListModel>() {
                    @Override
                    public void onResponse(Call<StringListModel> call, Response<StringListModel> response) {
                    progressDialog.dismiss();
                        try{
                            if(response.isSuccessful()) {
                                if ("Success".equalsIgnoreCase(response.body().getResult())) {
                                    final AlertBox alertBox = new AlertBox(getActivity());
                                    alertBox.showSearchableAlertBox();
                                    List<String> stateList = response.body().getData();
                                    alertBox.setStateList(stateList);
                                    alertBox.setCityList(new ArrayList<String>());
                                    alertBox.setDepList(new ArrayList<String>());
                                    alertBox.setOnMessageClickListener(new AlertBox.OnMessagePassClickListener() {
                                        @Override
                                        public void onClick(String state) {
                                            if(state.equals("Choose State"))
                                            {
                                                Toast.makeText(getActivity(), "Please Choose State", Toast.LENGTH_SHORT).show();
                                            }
                                            else {
                                                getCityList(alertBox, state);
                                            }
                                        }
                                    });

                                    alertBox.setOnCustomClickListener(new AlertBox.OnCustomClickListener() {
                                        @Override
                                        public void onClick() {
                                            getDepList(alertBox);
                                        }
                                    });

                                    alertBox.setOnValueClickListener(new AlertBox.OnMessageValuePassClickListener() {
                                        @Override
                                        public void onClick(AlertDialog alertDialog, String state, String city, String dep) {
                                            if(state.equals("Choose State"))
                                            {
                                                Toast.makeText(getActivity(), "Please Choose State", Toast.LENGTH_SHORT).show();
                                            }
                                            else if(city.equals("Choose City"))
                                            {
                                                Toast.makeText(getActivity(), "Please Choose City", Toast.LENGTH_SHORT).show();
                                            }
                                            else if(dep.equals("Choose Department"))
                                            {
                                                Toast.makeText(getActivity(), "Please Choose Department", Toast.LENGTH_SHORT).show();
                                            }
                                            else {
                                                getFullDepList(alertDialog,state,city,dep);

                                            }
                                        }
                                    });

                                }
                                else {
                                  AlertBox alertBox = new AlertBox(getActivity());
                                  alertBox.showAlertBox(getString(R.string.no_record),View.GONE);
                                }

                            }
                            else {
                                AlertBox alertBox = new AlertBox(getActivity());
                                alertBox.showAlertBox(getString(R.string.invalid_url),View.GONE);
                            }

                        }
                        catch (Exception e)
                        {
                            progressDialog.dismiss();
                            e.printStackTrace();
                            AlertBox alertBox = new AlertBox(getActivity());
                            alertBox.showAlertBox(getString(R.string.invalid_response),View.GONE);

                        }
                    }

                    @Override
                    public void onFailure(Call<StringListModel> call, Throwable t) {
                        progressDialog.dismiss();
                        AlertBox alertBox = new AlertBox(getActivity());
                        alertBox.showAlertBox(getString(R.string.failed_to_reach_server),View.GONE);

                    }
                });
            }
            catch (Exception e)
            {
                progressDialog.dismiss();
                AlertBox alertBox = new AlertBox(getActivity());
                alertBox.showAlertBox(getString(R.string.cannot_load_data),View.GONE);

            }
        }

    private void getCityList(final AlertBox defaultAlertBox, String state)
    {
        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(getActivity(), R.layout.custom_progress_bar);
        progressDialog.show();
        try{

            APIService service = RetroClient.getApiService();
            Call<StringListModel> call = service.cityList(state);
            call.enqueue(new Callback<StringListModel>() {
                @Override
                public void onResponse(Call<StringListModel> call, Response<StringListModel> response) {
                    progressDialog.dismiss();
                    try{
                        if(response.isSuccessful()) {
                            if ("Success".equalsIgnoreCase(response.body().getResult())) {
                                List<String> cityList = response.body().getData();
                                defaultAlertBox.setCityList(cityList);
                            }
                            else {
                                AlertBox alertBox = new AlertBox(getActivity());
                                alertBox.showAlertBox(getString(R.string.no_record),View.GONE);
                            }

                        }
                        else {
                            AlertBox alertBox = new AlertBox(getActivity());
                            alertBox.showAlertBox(getString(R.string.invalid_url),View.GONE);
                        }

                    }
                    catch (Exception e)
                    {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        AlertBox alertBox = new AlertBox(getActivity());
                        alertBox.showAlertBox(getString(R.string.invalid_response),View.GONE);

                    }
                }

                @Override
                public void onFailure(Call<StringListModel> call, Throwable t) {
                    progressDialog.dismiss();
                    AlertBox alertBox = new AlertBox(getActivity());
                    alertBox.showAlertBox(getString(R.string.failed_to_reach_server),View.GONE);

                }
            });
        }
        catch (Exception e)
        {
            progressDialog.dismiss();
            AlertBox alertBox = new AlertBox(getActivity());
            alertBox.showAlertBox(getString(R.string.cannot_load_data),View.GONE);

        }
    }

    private void getDepList(final AlertBox defaultAlertBox)
    {
        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(getActivity(), R.layout.custom_progress_bar);
        progressDialog.show();
        try{

            APIService service = RetroClient.getApiService();
            Call<StringListModel> call = service.depList();
            call.enqueue(new Callback<StringListModel>() {
                @Override
                public void onResponse(Call<StringListModel> call, Response<StringListModel> response) {
                    progressDialog.dismiss();
                    try{
                        if(response.isSuccessful()) {
                            if ("Success".equalsIgnoreCase(response.body().getResult())) {
                                List<String> depList = response.body().getData();
                                defaultAlertBox.setDepList(depList);
                            }
                            else {
                                AlertBox alertBox = new AlertBox(getActivity());
                                alertBox.showAlertBox(getString(R.string.no_record),View.GONE);
                            }

                        }
                        else {
                            AlertBox alertBox = new AlertBox(getActivity());
                            alertBox.showAlertBox(getString(R.string.invalid_url),View.GONE);
                        }

                    }
                    catch (Exception e)
                    {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        AlertBox alertBox = new AlertBox(getActivity());
                        alertBox.showAlertBox(getString(R.string.invalid_response),View.GONE);

                    }
                }

                @Override
                public void onFailure(Call<StringListModel> call, Throwable t) {
                    progressDialog.dismiss();
                    AlertBox alertBox = new AlertBox(getActivity());
                    alertBox.showAlertBox(getString(R.string.failed_to_reach_server),View.GONE);

                }
            });
        }
        catch (Exception e)
        {
            progressDialog.dismiss();
            AlertBox alertBox = new AlertBox(getActivity());
            alertBox.showAlertBox(getString(R.string.cannot_load_data),View.GONE);

        }
    }

    private void getFullDepList(final AlertDialog defaultAlertBox, String state, String city, final String dep)
    {
        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(getActivity(), R.layout.custom_progress_bar);
        progressDialog.show();
        try{

            APIService service = RetroClient.getApiService();
            Call<DepartmentDetailsModel> call = service.getWareHouseData(state,city,dep);
            call.enqueue(new Callback<DepartmentDetailsModel>() {
                @Override
                public void onResponse(Call<DepartmentDetailsModel> call, Response<DepartmentDetailsModel> response) {
                    progressDialog.dismiss();
                    try{
                        if(response.isSuccessful()) {
                            if ("Success".equalsIgnoreCase(response.body().getResult())) {
                                defaultAlertBox.dismiss();
                                FragmentManager fragmentManager=getActivity().getSupportFragmentManager();
                                FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
                                WareHouseDetailsFragment viewMyNeedFragment=new WareHouseDetailsFragment();
                                viewMyNeedFragment.setWareHouseData(response.body().getData(),dep);
                                fragmentTransaction.replace(R.id.fragment_container,viewMyNeedFragment,"wareHouse");
                                fragmentManager.beginTransaction();
                                fragmentTransaction.addToBackStack(null);
                                fragmentTransaction.commit();
                            }
                            else {
                                AlertBox alertBox = new AlertBox(getActivity());
                                alertBox.showAlertBox(getString(R.string.no_record),View.GONE);
                            }

                        }
                        else {
                            AlertBox alertBox = new AlertBox(getActivity());
                            alertBox.showAlertBox(getString(R.string.invalid_url),View.GONE);
                        }

                    }
                    catch (Exception e)
                    {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        AlertBox alertBox = new AlertBox(getActivity());
                        alertBox.showAlertBox(getString(R.string.invalid_response),View.GONE);

                    }
                }

                @Override
                public void onFailure(Call<DepartmentDetailsModel> call, Throwable t) {
                    progressDialog.dismiss();
                    AlertBox alertBox = new AlertBox(getActivity());
                    alertBox.showAlertBox(getString(R.string.failed_to_reach_server),View.GONE);

                }
            });
        }
        catch (Exception e)
        {
            progressDialog.dismiss();
            AlertBox alertBox = new AlertBox(getActivity());
            alertBox.showAlertBox(getString(R.string.cannot_load_data),View.GONE);

        }
    }
}
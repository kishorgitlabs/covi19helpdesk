package com.brainmagic.covid19helpdesk.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.brainmagic.covid19helpdesk.Constants;
import com.brainmagic.covid19helpdesk.R;
import com.brainmagic.covid19helpdesk.adapter.ThemeColorAdapter;
import com.brainmagic.covid19helpdesk.alert.AlertBox;
import com.brainmagic.covid19helpdesk.alert.TransparentProgressDialog;
import com.brainmagic.covid19helpdesk.locationapi.GeocodeAsyncTask;
import com.brainmagic.covid19helpdesk.model.api.APIService;
import com.brainmagic.covid19helpdesk.model.api.RetroClient;
import com.brainmagic.covid19helpdesk.model.multiselect.ValueList;
import com.brainmagic.covid19helpdesk.model.needy.NeedyData;
import com.brainmagic.covid19helpdesk.model.needy.NeedyDropDown;
import com.brainmagic.covid19helpdesk.model.needyformresult.NeedyFormResult;
import com.brainmagic.covid19helpdesk.model.post.needypost.NeedyFormPost;
import com.brainmagic.covid19helpdesk.network.NetworkConnection;
import com.brainmagic.covid19helpdesk.spinner.MultiSelectionSpinner;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HelperFragment extends Fragment  {

    private TextView fragmenttitle;
    private NeedyData needyData;

    //location declare

    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 5000;

    // fastest updates interval - 5 sec
    // location updates will be received if another app is requesting the locations
    // than your app can handle
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = 5000;

    private static final int REQUEST_CHECK_SETTINGS = 100;
    private boolean permissionGranted=true;
    // bunch of location related apis
    private FusedLocationProviderClient mFusedLocationClient;
    private SettingsClient mSettingsClient;
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private LocationCallback mLocationCallback;
    private Location mCurrentLocation;
    private EditText needyname,needymobile,needyage,howmanypeople,stayingalone,needyaddress,needylandmark,needyotherspecify,needymedicine,needyqty,needyremark;
    private static final String TAG = "HelperFragment";
    private Button submit_bt;
    private String name,mobile,age,hwpeople,staycondition,landmark,otherspecify,medicine,qty,remark,address;
    private MultiSelectionSpinner health_condition, i_need;
    private NeedyFormPost needyFormPost;
    private Spinner gender,maritalstatus;
    private SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.helper_fragment, container, false);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

        sharedPref = getActivity().getSharedPreferences(Constants.sharedConfig, Context.MODE_PRIVATE);
        editor = sharedPref.edit();


        needyFormPost=new NeedyFormPost();
        fragmenttitle=getActivity().findViewById(R.id.header);
        needyaddress=view.findViewById(R.id.needyaddress);
        needyname=view.findViewById(R.id.needyname);
        needymobile=view.findViewById(R.id.needymobile);
        needyage=view.findViewById(R.id.needyage);
        howmanypeople=view.findViewById(R.id.howmanypeople);
        stayingalone=view.findViewById(R.id.stayingalone);
        needylandmark=view.findViewById(R.id.needylandmark);
        needyotherspecify=view.findViewById(R.id.needyotherspecify);
        needymedicine=view.findViewById(R.id.needymedicine);
        needyqty=view.findViewById(R.id.needyqty);
        needyremark=view.findViewById(R.id.needyremark);
        submit_bt=view.findViewById(R.id.submit_bt);
        gender=view.findViewById(R.id.gender);
        maritalstatus=view.findViewById(R.id.maritalstatus);
        fragmenttitle.setText(R.string.helper);
        health_condition=view.findViewById(R.id.health_condition);
        i_need=view.findViewById(R.id.i_need);

        String[] genderArray = getResources().getStringArray(R.array.gender);
        ThemeColorAdapter arrayAdapter = new ThemeColorAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item,genderArray);
        gender.setAdapter(arrayAdapter);

        final String[] maritalArray = getResources().getStringArray(R.array.martial_status);
        ThemeColorAdapter martials_arrayAdapter = new ThemeColorAdapter(getActivity(), R.layout.support_simple_spinner_dropdown_item,maritalArray);
        maritalstatus.setAdapter(martials_arrayAdapter);
        submit_bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                name=needyname.getText().toString();
                mobile=needymobile.getText().toString();
                age=needyage.getText().toString();
                hwpeople=howmanypeople.getText().toString();
                address=needyaddress.getText().toString();
                staycondition=stayingalone.getText().toString();
                landmark=needylandmark.getText().toString();
                otherspecify=needyotherspecify.getText().toString();
                medicine=needymedicine.getText().toString();
                qty=needyqty.getText().toString();
                remark=needyremark.getText().toString();


                if(maritalArray[0].equalsIgnoreCase(maritalstatus.getSelectedItem().toString()))
                {
                    needyFormPost.setMartialStatus("");
                }
                else {
                    needyFormPost.setMartialStatus(maritalstatus.getSelectedItem().toString());
                }

                if(TextUtils.isEmpty(name))
                {
                    Toast.makeText(getActivity(), getString(R.string.enter_name), Toast.LENGTH_LONG).show();
                }
                else if(TextUtils.isEmpty(mobile))
                {
                    Toast.makeText(getActivity(), getString(R.string.enter_mobile_no), Toast.LENGTH_LONG).show();
                }
                else if (gender.getSelectedItem().toString().equals("Select Gender")){
                    Toast.makeText(getActivity(), getString(R.string.choose_gender), Toast.LENGTH_LONG).show();
                }
                else if(needyFormPost.getINeedList() == null)
                {
                    Toast.makeText(getActivity(), getString(R.string.choose_i_need), Toast.LENGTH_LONG).show();
                }
                else if(needyFormPost.getINeedList().size() == 0)
                {
                    Toast.makeText(getActivity(), getString(R.string.choose_i_need), Toast.LENGTH_LONG).show();
                }



                else {
                    needyFormPost.setGender(gender.getSelectedItem().toString());
                    needyFormPost.setName(name);
                    needyFormPost.setMobileNo(mobile);
                    needyFormPost.setGender(age);
                    needyFormPost.setHowManyPeople(hwpeople);
                    needyFormPost.setLandMark(landmark);
                    needyFormPost.setMedicineName(medicine);
                    needyFormPost.setQuantity(qty);
                    needyFormPost.setStayingAlone(staycondition);
                    needyFormPost.setRemark(remark);
                    needyFormPost.setAddress(address);
                    needyFormPost.setRegId(sharedPref.getString("tokenId",""));
                    needyPostData();
                }

            }
        });

        health_condition.setListener(new MultiSelectionSpinner.OnMultipleItemsSelectedListener() {
            @Override
            public void selectedIndices(List<Integer> indices) {

            }

            @Override
            public void selectedStrings(List<ValueList> strings) {
                needyFormPost.setHealthConditionList(strings);
                Log.d(TAG, "selectedStrings: "+strings);

            }
        });

        i_need.setListener(new MultiSelectionSpinner.OnMultipleItemsSelectedListener() {
            @Override
            public void selectedIndices(List<Integer> indices) {

            }

            @Override
            public void selectedStrings(List<ValueList> strings) {
                needyFormPost.setINeedList(strings);
                if(i_need.isOthers("Others"))
                {
                    needyotherspecify.setVisibility(View.VISIBLE);
                }
                else
                {
                    needyotherspecify.setVisibility(View.GONE);
                }
                if(i_need.isOthers("Emergency Medicine"))
                {
                    needymedicine.setVisibility(View.VISIBLE);
                }
                else
                {
                    needymedicine.setVisibility(View.GONE);
                }
                Log.d(TAG, "selectedStrings: "+strings);
            }
        });
        getCurrentLocation();

//        dropDownData.getTypeofBusiness();
        checkInternet();
        return view;
    }

    //STEP 1
    private void getCurrentLocation()
    {
        NetworkConnection connection = new NetworkConnection(getActivity());
        if(connection.checkInternet()) {
            initPermission();
        }
        else {
            AlertBox alertBox = new AlertBox(getActivity());
            alertBox.showAlertBox(getString(R.string.no_internet_connection),View.GONE);

        }
    }

    //step 2
    private void initPermission() {
        if(PackageManager.PERMISSION_GRANTED != ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION))
        {
            ActivityCompat.requestPermissions(getActivity(),new String[]{Manifest.permission.ACCESS_FINE_LOCATION},100);
        }
        else {
            permissionGranted=true;
            init();
            startLocationUpdate();
        }
    }

    //step 3

    private void init() {
        if(permissionGranted) {
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
            mSettingsClient = LocationServices.getSettingsClient(getActivity());

            mLocationCallback = new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {
                    super.onLocationResult(locationResult);
                    // location is received
                    mCurrentLocation = locationResult.getLastLocation();
//                    mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
                    getAddressFromLocation();
                }
            };

            mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
            mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
            builder.addLocationRequest(mLocationRequest);
            mLocationSettingsRequest = builder.build();
//        startLocationButtonClick();
        }
    }

//step 4
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Log.e(TAG, "User agreed to make required location settings changes.");
                        // Nothing to do. startLocationupdates() gets called in onResume again.
                        if(permissionGranted)
                        {
                            startLocationUpdate();
                        }
                        else {
                            initPermission();
                        }
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.e(TAG, "User choose not to make required location settings changes.");
                        break;
                }
                break;
        }
    }

    //step 5
    //to check location is enabled or not
    private void startLocationUpdate() {
        mSettingsClient
                .checkLocationSettings(mLocationSettingsRequest)
                .addOnSuccessListener(getActivity(), new OnSuccessListener<LocationSettingsResponse>() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        Log.i(TAG, "All location settings are satisfied.");

//                        Toast.makeText(getActivity(), "Started location updates!", Toast.LENGTH_SHORT).show();

                        //noinspection MissingPermission
                        mFusedLocationClient.requestLocationUpdates(mLocationRequest,
                                mLocationCallback, Looper.myLooper());
//                        LocationManager manager= (LocationManager) getSystemService(LOCATION_SERVICE);

                        getAddressFromLocation();
                    }
                })
                .addOnFailureListener(getActivity(), new OnFailureListener() {
                    @Override
                    public void onFailure(Exception e) {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                Log.i(TAG, "Location settings are not satisfied. Attempting to upgrade " +
                                        "location settings ");
                                try {
                                    // Show the dialog by calling startResolutionForResult(), and check the
                                    // result in onActivityResult().
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    rae.startResolutionForResult(getActivity(), REQUEST_CHECK_SETTINGS);
                                } catch (IntentSender.SendIntentException sie) {
                                    Log.i(TAG, "PendingIntent unable to execute request.");
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                String errorMessage = "Location settings are inadequate, and cannot be " +
                                        "fixed here. Fix in Settings.";
                                Log.e(TAG, errorMessage);

                                Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_LONG).show();
                                break;
                            case LocationSettingsStatusCodes.SUCCESS:
                                permissionGranted=true;
                                init();
                        }
                        getAddressFromLocation();
                    }
                });
    }

    //step 6

    public void stopLocationUpdates() {
        // Removing location updates
        if(mLocationCallback!=null)
            mFusedLocationClient
                    .removeLocationUpdates(mLocationCallback)
                    .addOnCompleteListener(getActivity(), new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(Task<Void> task) {
                            Log.e(TAG, "onComplete: location stopped" );
//                            Toast.makeText(getActivity(), "Location updates stopped!", Toast.LENGTH_SHORT).show();
                            //                        toggleButtons();
                        }
                    });
    }


    //step 7
    private void getAddressFromLocation(){
        if(mCurrentLocation !=null) {
            GeocodeAsyncTask geocodeAsyncTask = new GeocodeAsyncTask(getActivity());
            geocodeAsyncTask.execute(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
        }
    }


    //update ui
    public void updateLocationUI(Address address)
    {
        needyaddress.setText(address.getAddressLine(0));
//        if(isSameAsAbove) {
//            doorNoEt.setText(address.getPremises());
//            cityEt.setText(address.getLocality());
//            stateEt.setText(address.getAdminArea());
//            streetEt.setText(address.getThoroughfare());
////         = address.getSubLocality();
//            pinEt.setText(address.getPostalCode());
//            isSameAsAbove = !isSameAsAbove;
//        }
    }



    private void checkInternet()
    {
        NetworkConnection connection = new NetworkConnection(getActivity());
        if(connection.checkInternet()) {
            getNeedy();
        }
        else {
            AlertBox alertBox = new AlertBox(getActivity());
            alertBox.showAlertBox(getString(R.string.no_internet_connection),View.GONE);

        }
    }


    private void getNeedy()
    {
        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(getActivity(), R.layout.custom_progress_bar);
        progressDialog.show();
        try{

            APIService service = RetroClient.getApiService();
            Call<NeedyDropDown> call = service.getNeedyData();
            call.enqueue(new Callback<NeedyDropDown>() {
                @Override
                public void onResponse(Call<NeedyDropDown> call, Response<NeedyDropDown> response) {
                    progressDialog.dismiss();
                    try{

                        if("Success".equalsIgnoreCase(response.body().getResult()))
                        {
                            needyData=response.body().getData();
                            health_condition.setItems(needyData.getHealthCondition());
                            health_condition.setHint(getString(R.string.healthcondition));
                            health_condition.setSelection(needyData.getHealthCondition().size());

                            i_need.setItems(needyData.getINeedy());
                            i_need.setHint(getString(R.string.ineed));
                            i_need.setSelection(needyData.getINeedy().size());

                        }
                        else {
                            AlertBox alertBox = new AlertBox(getActivity());
                            alertBox.showAlertBoxWithListener(getString(R.string.nolinks),View.GONE);
                            alertBox.setOnPositiveClickListener(new AlertBox.OnPositiveClickListener() {
                                @Override
                                public void onPositiveClick() {
                                    getActivity().onBackPressed();
                                }
                            });
                        }

                    }
                    catch (Exception e)
                    {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        AlertBox alertBox = new AlertBox(getActivity());
                        alertBox.showAlertBoxWithListener(getString(R.string.invalid_response),View.GONE);
                        alertBox.setOnPositiveClickListener(new AlertBox.OnPositiveClickListener() {
                            @Override
                            public void onPositiveClick() {
                                getActivity().onBackPressed();
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Call<NeedyDropDown> call, Throwable t) {
                    progressDialog.dismiss();
                    AlertBox alertBox = new AlertBox(getActivity());
                    alertBox.showAlertBoxWithListener(getString(R.string.failed_to_reach_server),View.GONE);
                    alertBox.setOnPositiveClickListener(new AlertBox.OnPositiveClickListener() {
                        @Override
                        public void onPositiveClick() {
                            getActivity().onBackPressed();
                        }
                    });
                }
            });
        }catch (Exception e)
        {
            progressDialog.dismiss();
            AlertBox alertBox = new AlertBox(getActivity());
            alertBox.showAlertBoxWithListener(getString(R.string.cannot_load_data),View.GONE);
            alertBox.setOnPositiveClickListener(new AlertBox.OnPositiveClickListener() {
                @Override
                public void onPositiveClick() {
                    getActivity().onBackPressed();
                }
            });
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        stopLocationUpdates();
    }

    private void needyPostData()
    {
        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(getActivity(), R.layout.custom_progress_bar);
        progressDialog.show();
        try {
            APIService apiService = RetroClient.getApiService();
            Call<NeedyFormResult> call = apiService.needyPost(needyFormPost);
            Log.i(TAG, "needyPostData: "+needyFormPost);
            call.enqueue(new Callback<NeedyFormResult>() {
                @Override
                public void onResponse(Call<NeedyFormResult> call, Response<NeedyFormResult> response) {
                    progressDialog.dismiss();
                    try{
                        if(response.isSuccessful())
                        {
                            if("Success".equalsIgnoreCase(response.body().getResult())) {
                                AlertBox alertBox = new AlertBox(getActivity());
                                alertBox.showAlertBoxWithListener(getString(R.string.needydetailsposted), View.GONE);
                                alertBox.setOnPositiveClickListener(new AlertBox.OnPositiveClickListener() {
                                    @Override
                                    public void onPositiveClick() {
                                        getActivity().onBackPressed();
                                    }
                                });
                            }
                            else {
                                AlertBox alertBox = new AlertBox(getActivity());
                                alertBox.showAlertBox(getString(R.string.tryagainlater), View.GONE);
                            }
                        }
                        else {
                            AlertBox alertBox = new AlertBox(getActivity());
                            alertBox.showAlertBox(getString(R.string.invalid_url), View.GONE);
                        }
                    }
                    catch (Exception e)
                    {
                        AlertBox alertBox = new AlertBox(getActivity());
                        alertBox.showAlertBox(getString(R.string.invalid_response), View.GONE);
                    }
                }

                @Override
                public void onFailure(Call<NeedyFormResult> call, Throwable t) {
                    progressDialog.dismiss();
                    AlertBox alertBox = new AlertBox(getActivity());
                    alertBox.showAlertBox(getString(R.string.failed_to_reach_server), View.GONE);
                }
            });

        }
        catch (Exception e)
        {
            progressDialog.dismiss();
            AlertBox alertBox = new AlertBox(getActivity());
            alertBox.showAlertBox(getString(R.string.cannot_connect_to_server), View.GONE);
        }
    }

        }
package com.brainmagic.covid19helpdesk.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.brainmagic.covid19helpdesk.R;
import com.brainmagic.covid19helpdesk.adapter.UseFullLInksAdapter;
import com.brainmagic.covid19helpdesk.alert.AlertBox;
import com.brainmagic.covid19helpdesk.alert.TransparentProgressDialog;
import com.brainmagic.covid19helpdesk.model.api.APIService;
import com.brainmagic.covid19helpdesk.model.api.RetroClient;
import com.brainmagic.covid19helpdesk.model.usefulllinks.UseFullData;
import com.brainmagic.covid19helpdesk.model.usefulllinks.UseFullLinksResult;
import com.brainmagic.covid19helpdesk.network.NetworkConnection;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UseFullLinksFragment extends Fragment {

    private TextView fragmenttitle;
    private RecyclerView usefullinkslist;
    private List<UseFullData> useFullDataList;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);

        View view = LayoutInflater.from(getActivity()).inflate(R.layout.useful_links, container, false);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

        usefullinkslist=view.findViewById(R.id.usefullinkslist);
        fragmenttitle=getActivity().findViewById(R.id.header);
        fragmenttitle.setText(R.string.usefull);

        usefullinkslist.setLayoutManager(new LinearLayoutManager(getActivity()));
        checkInternet();
        return view;
    }

    private void checkInternet()
    {
        NetworkConnection connection = new NetworkConnection(getActivity());
        if(connection.checkInternet()) {
            getUseFullLInks();
        }
        else {
            AlertBox alertBox = new AlertBox(getActivity());
            alertBox.showAlertBox(getString(R.string.no_internet_connection),View.GONE);

        }
    }



    private void getUseFullLInks()
    {
        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(getActivity(), R.layout.custom_progress_bar);
        progressDialog.show();
        try{

            APIService service = RetroClient.getApiService();
            Call<UseFullLinksResult> call = service.getUseFull();
            call.enqueue(new Callback<UseFullLinksResult>() {
                @Override
                public void onResponse(Call<UseFullLinksResult> call, Response<UseFullLinksResult> response) {
                    progressDialog.dismiss();
                    try{

                        if("Success".equalsIgnoreCase(response.body().getResult()))
                        {
                            useFullDataList=response.body().getData();
                            UseFullLInksAdapter useFullLInksAdapter=new UseFullLInksAdapter(getActivity(),useFullDataList);
                            usefullinkslist.setAdapter(useFullLInksAdapter);
                        }
                        else {
                            AlertBox alertBox = new AlertBox(getActivity());
                            alertBox.showAlertBoxWithListener(getString(R.string.no_record),View.GONE);
                            alertBox.setOnPositiveClickListener(new AlertBox.OnPositiveClickListener() {
                                @Override
                                public void onPositiveClick() {
                                    getActivity().onBackPressed();
                                }
                            });
                        }

                    }
                    catch (Exception e)
                    {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        AlertBox alertBox = new AlertBox(getActivity());
                        alertBox.showAlertBoxWithListener(getString(R.string.invalid_response),View.GONE);
                        alertBox.setOnPositiveClickListener(new AlertBox.OnPositiveClickListener() {
                            @Override
                            public void onPositiveClick() {
                                getActivity().onBackPressed();
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Call<UseFullLinksResult> call, Throwable t) {
                    progressDialog.dismiss();
                    AlertBox alertBox = new AlertBox(getActivity());
                    alertBox.showAlertBoxWithListener(getString(R.string.failed_to_reach_server),View.GONE);
                    alertBox.setOnPositiveClickListener(new AlertBox.OnPositiveClickListener() {
                        @Override
                        public void onPositiveClick() {
                            getActivity().onBackPressed();
                        }
                    });
                }
            });
        }catch (Exception e)
        {
            progressDialog.dismiss();
            AlertBox alertBox = new AlertBox(getActivity());
            alertBox.showAlertBoxWithListener(getString(R.string.cannot_load_data),View.GONE);
            alertBox.setOnPositiveClickListener(new AlertBox.OnPositiveClickListener() {
                @Override
                public void onPositiveClick() {
                    getActivity().onBackPressed();
                }
            });
        }

    }
}
package com.brainmagic.covid19helpdesk.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.brainmagic.covid19helpdesk.Constants;
import com.brainmagic.covid19helpdesk.R;
import com.brainmagic.covid19helpdesk.alert.AlertBox;
import com.brainmagic.covid19helpdesk.alert.TransparentProgressDialog;
import com.brainmagic.covid19helpdesk.fragment.HelperFragment;
import com.brainmagic.covid19helpdesk.fragment.HomeFragment;
import com.brainmagic.covid19helpdesk.fragment.PersonalDetailFragment;
import com.brainmagic.covid19helpdesk.fragment.SignupFragment;
import com.brainmagic.covid19helpdesk.fragment.WaitForApprovalFragment;
import com.brainmagic.covid19helpdesk.locationapi.GeocodeAsyncTask;
import com.brainmagic.covid19helpdesk.model.api.APIService;
import com.brainmagic.covid19helpdesk.model.api.RetroClient;
import com.brainmagic.covid19helpdesk.model.registration.RegistrationModel;
import com.brainmagic.covid19helpdesk.network.NetworkConnection;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements GeocodeAsyncTask.AddressListener {

    private Fragment fragment;
    private SignupFragment signupFragment;
    private SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharedPref = getSharedPreferences(Constants.sharedConfig, Context.MODE_PRIVATE);
        editor = sharedPref.edit();

        if(sharedPref.getBoolean("isApproved", false))
        {
            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            fragment = new HomeFragment();
            transaction.add(R.id.fragment_container,fragment,"home");
            transaction.commit();
        }
        else if(sharedPref.getBoolean("isFileUploaded", false))
        {
            NetworkConnection connection = new NetworkConnection(this);
            if(connection.checkInternet())
            {
                checkApproval();
            }
            else {
                AlertBox alertBox = new AlertBox(this);
                alertBox.showAlertBoxWithListener(getString(R.string.no_internet_connection), View.GONE);
                alertBox.setOnPositiveClickListener(new AlertBox.OnPositiveClickListener() {
                    @Override
                    public void onPositiveClick() {
                        finish();
                    }
                });
            }

        }
        else if(sharedPref.getBoolean("isRegistered", false))
        {
            NetworkConnection connection = new NetworkConnection(this);
            if(connection.checkInternet())
            {
                checkApproval();
            }
            else {
                AlertBox alertBox = new AlertBox(this);
                alertBox.showAlertBoxWithListener(getString(R.string.no_internet_connection), View.GONE);
                alertBox.setOnPositiveClickListener(new AlertBox.OnPositiveClickListener() {
                    @Override
                    public void onPositiveClick() {
                        finish();
                    }
                });
            }
        }
        else {
            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            signupFragment = new SignupFragment();
            fragment = signupFragment;
            transaction.add(R.id.fragment_container,fragment,"signUp");
            transaction.commit();
        }


//        if(sharedPref.getBoolean("isFileUploaded", false))
//        {
//            if(sharedPref.getBoolean("isApproved", false))
//            {
//                FragmentManager manager = getSupportFragmentManager();
//                FragmentTransaction transaction = manager.beginTransaction();
//                fragment = new HomeFragment();
//                transaction.add(R.id.fragment_container,fragment,"signUp");
//                transaction.commit();
//            }
//            else {
//
//                NetworkConnection connection = new NetworkConnection(this);
//                if(connection.checkInternet())
//                {
//                    checkApproval();
//                }
//                else {
//                    AlertBox alertBox = new AlertBox(this);
//                    alertBox.showAlertBoxWithListener(getString(R.string.no_internet_connection), View.GONE);
//                    alertBox.setOnPositiveClickListener(new AlertBox.OnPositiveClickListener() {
//                        @Override
//                        public void onPositiveClick() {
//                            finish();
//                        }
//                    });
//                }
//
//            }
//
//        }
//        else if(sharedPref.getBoolean("isRegistered", false))
//        {
//            FragmentManager manager = getSupportFragmentManager();
//            FragmentTransaction transaction = manager.beginTransaction();
//            fragment = new PersonalDetailFragment();
//            transaction.add(R.id.fragment_container,fragment,"signUp");
//            transaction.commit();
//        }
//        else {
//            FragmentManager manager = getSupportFragmentManager();
//            FragmentTransaction transaction = manager.beginTransaction();
//            signupFragment = new SignupFragment();
//            fragment = signupFragment;
//            transaction.add(R.id.fragment_container,fragment,"signUp");
//            transaction.commit();
//        }


    }



    private void checkApproval()
    {
        final TransparentProgressDialog progressDialog = new TransparentProgressDialog(MainActivity.this, R.layout.custom_progress_bar);
        progressDialog.show();
        try{

            APIService service = RetroClient.getApiService();
            Call<RegistrationModel> call = service.approvalAcknowledge(sharedPref.getString("tokenId",""),
                    String.valueOf(sharedPref.getInt("id", 0)));
            call.enqueue(new Callback<RegistrationModel>() {
                @Override
                public void onResponse(Call<RegistrationModel> call, Response<RegistrationModel> response) {
                    progressDialog.dismiss();
                    try{
                        if(response.isSuccessful())
                            if("Success".equalsIgnoreCase(response.body().getResult()))
                            {
                                editor.putBoolean("isApproved", true);
                                editor.commit();
                                FragmentManager manager = getSupportFragmentManager();
                                FragmentTransaction transaction = manager.beginTransaction();
                                fragment = new HomeFragment();
                                transaction.add(R.id.fragment_container,fragment,"home");
                                transaction.commit();
                            }
                            else
                            {
                                if(sharedPref.getBoolean("isFileUploaded", false))
                                {
                                    FragmentManager manager = getSupportFragmentManager();
                                    FragmentTransaction transaction = manager.beginTransaction();
                                    fragment = new WaitForApprovalFragment();
                                    transaction.add(R.id.fragment_container,fragment,"wait");
                                    transaction.commit();
                                }
                                else {
                                    FragmentManager manager = getSupportFragmentManager();
                                    FragmentTransaction transaction = manager.beginTransaction();
                                    fragment = new PersonalDetailFragment();
                                    transaction.add(R.id.fragment_container, fragment, "personal");
                                    transaction.commit();
                                }
                            }
                            else {
                                    AlertBox alertBox = new AlertBox(MainActivity.this);
                                    alertBox.showAlertBoxWithListener(getString(R.string.invalid_url), View.GONE);
                                    alertBox.setOnPositiveClickListener(new AlertBox.OnPositiveClickListener() {
                                        @Override
                                        public void onPositiveClick() {
                                            finish();
                                        }
                                    });
                        }


                    }
                    catch (Exception e)
                    {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        AlertBox alertBox = new AlertBox(MainActivity.this);
                        alertBox.showAlertBox(getString(R.string.invalid_response),View.GONE);
//                        Toast.makeText(MainActivity.this, getString(R.string.invalid_response), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<RegistrationModel> call, Throwable t) {
                    progressDialog.dismiss();
                    AlertBox alertBox = new AlertBox(MainActivity.this);
                    alertBox.showAlertBox(getString(R.string.failed_to_reach_server),View.GONE);
//                    Toast.makeText(MainActivity.this, getString(R.string.failed_to_reach_server), Toast.LENGTH_SHORT).show();
                }
            });
        }catch (Exception e)
        {
            progressDialog.dismiss();
            AlertBox alertBox = new AlertBox(MainActivity.this);
            alertBox.showAlertBox(getString(R.string.cannot_load_data),View.GONE);
//            Toast.makeText(MainActivity.this, getString(R.string.cannot_load_data), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode,resultCode,data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onRequestPermissionsResult(requestCode,permissions,grantResults);
        }
    }

    @Override
    public void addressListener(Address address) {

        try {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
            String tag = fragment.getTag();
            if (tag.equals("signUp")) {
                SignupFragment signupFragment = (SignupFragment) fragment;
                signupFragment.updateLocationUI(address);
            } else if (tag.equals("helper")) {
                HelperFragment helperFragment = (HelperFragment) fragment;
                helperFragment.updateLocationUI(address);
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
